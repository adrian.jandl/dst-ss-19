package dst.ass1.kv.impl;

import dst.ass1.kv.ISessionManager;
import dst.ass1.kv.SessionCreationFailedException;
import dst.ass1.kv.SessionNotFoundException;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SessionManager implements ISessionManager {

    private Jedis jedis;

    public SessionManager(Jedis jedis) {
        this.jedis = jedis;
    }

    @Override
    public String createSession(Long userId, int timeToLive) throws SessionCreationFailedException {
        String uuid = String.valueOf(UUID.randomUUID());
//        jedis.set(uuid, String.valueOf(userId));
        jedis.setex(String.valueOf(userId), timeToLive, uuid);
        Map<String, String> map = new HashMap<>();
        map.put("userId", String.valueOf(userId));
        jedis.hmset(uuid, map);
        jedis.expire(uuid, timeToLive);

        return uuid;
    }

    @Override
    public void setSessionVariable(String sessionId, String key, String value) throws SessionNotFoundException {
        if (jedis.exists(sessionId)) {
            Map<String, String> map = new HashMap<>();
            map.put(key, value);
            jedis.hmset(sessionId, map);
        } else {
            throw new SessionNotFoundException("Session not found");
        }
    }

    @Override
    public String getSessionVariable(String sessionId, String key) throws SessionNotFoundException {
        if (jedis.exists(sessionId)) {
            return jedis.hget(sessionId, key);
        } else {
            throw new SessionNotFoundException("Session not found");
        }
    }

    @Override
    public Long getUserId(String sessionId) throws SessionNotFoundException {
        String userId = jedis.hget(sessionId, "userId");
        if (userId == null) {
            throw new SessionNotFoundException("Session not found");
        }
        return Long.valueOf(userId);
    }

    @Override
    public int getTimeToLive(String sessionId) throws SessionNotFoundException {
        Long ttl = jedis.ttl(sessionId);
        //from doc: If the Key does not exists or does not have an associated expire, -1 is returned.
        if (ttl == null || ttl < 0) {
            throw new SessionNotFoundException("Session not found");
        }
        return Math.toIntExact(ttl);
    }

    @Override
    public String requireSession(Long userId, int timeToLive) throws SessionCreationFailedException {
        if (!jedis.exists(String.valueOf(userId))) {
            return createSession(userId, timeToLive);
        }
        jedis.expire(String.valueOf(userId), timeToLive);
        return jedis.get(String.valueOf(userId));
    }

    @Override
    public void close() {
        jedis.close();
    }
}
