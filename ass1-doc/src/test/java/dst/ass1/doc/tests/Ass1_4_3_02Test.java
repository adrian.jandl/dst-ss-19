package dst.ass1.doc.tests;

import dst.ass1.doc.DocumentTestData;
import dst.ass1.doc.EmbeddedMongo;
import dst.ass1.doc.MongoService;
import org.bson.Document;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.*;

public class Ass1_4_3_02Test {

    @ClassRule
    public static EmbeddedMongo embeddedMongo = new EmbeddedMongo();

    @Rule
    public MongoService mongo = new MongoService(new DocumentTestData());

    @Test
    public void getDocumentStatistics_returnsCorrectStatistics() throws Exception {
        List<Document> documentStatistics = mongo.getDocumentQuery().getDocumentStatistics();
        assertNotNull(documentStatistics);
        assertEquals(3, documentStatistics.size());

        List<String> types = documentStatistics.stream().map(d -> d.getString("_id")).collect(Collectors.toList());
        assertThat("expected three aggregation keys", types, hasItems("Restaurant", "Park", "University"));

        Map<String, Double> dsMap = documentStatistics.stream().collect(Collectors.toMap(
            d -> d.getString("_id"),
            d -> d.getDouble("value"))
        );

        assertEquals(4.0d, dsMap.get("Restaurant"), 0);
        assertEquals(1.0d, dsMap.get("Park"), 0);
        assertEquals(7.0d, dsMap.get("University"), 0);
    }

}
