package dst.ass1.doc.impl;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Indexes;
import dst.ass1.doc.IDocumentRepository;
import dst.ass1.jpa.model.ILocation;
import dst.ass1.jpa.util.Constants;
import org.bson.Document;

import java.util.Map;

public class DocumentRepository implements IDocumentRepository {

    private MongoDatabase mongoDatabase;

    public DocumentRepository() {
        MongoClient mongoClient = new MongoClient("127.0.0.1");
        mongoDatabase = mongoClient.getDatabase(Constants.MONGO_DB_NAME);
        MongoCollection<Document> collection = mongoDatabase.getCollection(Constants.COLL_LOCATION_DATA);
        collection.createIndex(Indexes.ascending("location_id"));
        collection.createIndex(Indexes.ascending("geo"));
    }

    @Override
    public void insert(ILocation location, Map<String, Object> locationProperties) {
        Document document = new Document();

        locationProperties.put("name", location.getName());
        locationProperties.put("location_id", location.getLocationId());

        MongoCollection<Document> collection = mongoDatabase.getCollection(Constants.COLL_LOCATION_DATA);
        document.putAll(locationProperties);
        collection.insertOne(document);
    }
}
