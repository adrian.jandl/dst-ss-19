package dst.ass1.doc.impl;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import dst.ass1.doc.IDocumentQuery;
import dst.ass1.jpa.util.Constants;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;

public class DocumentQuery implements IDocumentQuery {
    private MongoDatabase database;

    public DocumentQuery(MongoDatabase database) {
        this.database = database;
    }

    @Override
    public Document findLocationById(Long locationId) {
        MongoCollection<Document> collection = database.getCollection(Constants.COLL_LOCATION_DATA);
        return collection.find(eq("location_id", locationId)).first();
    }

    @Override
    public List<Long> findIdsByNameAndRadius(String name, double longitude, double latitude, double radius) {
        MongoCollection<Document> collection = database.getCollection(Constants.COLL_LOCATION_DATA);

        List<Long> retList = new ArrayList<>();
        Iterable<Long> iterable = collection
                .find(and(
                        regex("name", ".*" + name + "-*"),
                        geoWithinCenterSphere("geo", longitude, latitude, radius / (6378.1 * 1000)))) //m to rad
                .projection(fields(include("location_id")))
                .map(document -> document.getLong("location_id"));

        iterable.forEach(retList::add);

        return retList;

    }

    @Override
    public List<Document> getDocumentStatistics() {
        MongoCollection<Document> collection = database.getCollection(Constants.COLL_LOCATION_DATA);
        String map = "function() { if(this.type==='place'){emit(this.category, {count: 1})}}";
        String reduce = "function(key, values){ " +
                "var total = 0; " +
                "for(var i in values){total++;}" +
                "return {count: total}" +
                "}";

        Iterable<Document> it = collection.mapReduce(map, reduce);

        List<Document> returnList = new ArrayList<>();

        it.forEach(item -> {
            Document doc = new Document();
            doc.put("_id", item.get("_id"));
            doc.put("value", ((Document) item.get("value")).get("count"));
            returnList.add(doc);
        });
        return returnList;
    }
}
