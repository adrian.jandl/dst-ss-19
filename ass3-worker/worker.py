#!/usr/bin/env python

import json
import pika
import random
import redis
import signal
import sys
import time
from haversine import haversine

print("starting container")
region = sys.argv[1]
print(region)

current_milli_time = lambda: int(round(time.time() * 1000))
exchange = 'dst.workers'

vmhost = '192.168.99.99'
r = redis.Redis(host=vmhost, port=6379)


def distance_between(one, two):
    return haversine(one, two)


def closest_driver(currentPos):
    drivers = r.hgetall('drivers:' + region)


def get_sleep_time(region):
    if region == 'at_linz':
        return random.randint(1, 3)
    elif region == 'at_vienna':
        return random.randint(3, 6)
    elif region == 'de_berlin':
        return random.randint(8, 12)
    else:
        raise Exception('invalid region found')


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    millis = current_milli_time()
    print("Time before request %r" % millis)
    request = json.loads(body)

    sleep_time = get_sleep_time(region)
    print("Sleeping %r s" % sleep_time)
    time.sleep(get_sleep_time(region))

    response = {
        "requestId": "",
        "processingTime": current_milli_time() - millis,
        "driverId": closest_driver((request["pickup"]["latitude"], request["pickup"]["longitude"]))
    }

    ch.basic_publish(exchange=exchange, routing_key=region, body=json.dumps(response))


credentials = pika.PlainCredentials('dst', 'dst')
parameters = pika.ConnectionParameters(vmhost,
                                       5672,
                                       '/',
                                       credentials)

connection = pika.BlockingConnection(parameters)


def handler(number, stackframe):
    print("got " + str(number))
    connection.close()
    sys.exit(0)


signal.signal(signal.SIGTERM, handler)
signal.signal(signal.SIGINT, handler)

channel = connection.channel()

channel.queue_declare(queue=region)
channel.basic_consume(queue=region, auto_ack=True, on_message_callback=callback)

channel.start_consuming()
