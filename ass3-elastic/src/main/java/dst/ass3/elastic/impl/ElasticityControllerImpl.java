package dst.ass3.elastic.impl;

import dst.ass3.elastic.ContainerException;
import dst.ass3.elastic.ContainerInfo;
import dst.ass3.elastic.IContainerService;
import dst.ass3.elastic.IElasticityController;
import dst.ass3.messaging.IWorkloadMonitor;
import dst.ass3.messaging.Region;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ElasticityControllerImpl implements IElasticityController {
    private IContainerService containerService;
    private IWorkloadMonitor workloadMonitor;

    private Map<Region, Long> maxWaitTime = new HashMap<>();

    private static double ALPHA = 0.1;
    private static double OMEGA = 0.05;


    public ElasticityControllerImpl(IContainerService containerService, IWorkloadMonitor workloadMonitor) {
        this.containerService = containerService;
        this.workloadMonitor = workloadMonitor;

        maxWaitTime.put(Region.DE_BERLIN, 120 * 1000L);
        maxWaitTime.put(Region.AT_VIENNA, 30 * 1000L);
        maxWaitTime.put(Region.AT_LINZ, 30 * 1000L);
    }

    @Override
    public void adjustWorkers() throws ContainerException {
        Arrays.stream(Region.values()).forEach(r -> {
            int outByHowMuch = shouldScaleOutByHowMuch(r);
            if (outByHowMuch > 0) {
                for (int i = 0; i < outByHowMuch; i++) {
                    try {
                        containerService.startWorker(r);
                    } catch (ContainerException e) {
                        e.printStackTrace();
                    }
                }
                return;
            }

            int downByHowMuch = shouldScaleDownByHowMuch(r);
            if (downByHowMuch > 0) {
                try {
                    List<ContainerInfo> containers = containerService.listContainers();
                    for (int i = 0; i < downByHowMuch; i++) {
                        containers
                                .stream()
                                .filter(c -> c.getWorkerRegion() == r)
                                .findFirst()
                                .ifPresent(c -> {
                                    try {
                                        containerService.stopContainer(c.getContainerId());
                                    } catch (ContainerException e) {
                                        e.printStackTrace();
                                    }
                                    containers.remove(c);
                                });
                    }
                } catch (ContainerException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private int shouldScaleDownByHowMuch(Region r) {
        int amountToRemove = 0;
        long oldWorkers = getWorkerCountByRegion(r);
        if (shouldScaleDownInitial(calculateExpectedWaitTime(oldWorkers, getNumberOfWaitingRequestByRegion(r), getAverageWaitTimeByRegion(r)), maxWaitTime.get(r))) {
            while (shouldScaleDown(calculateExpectedWaitTime(oldWorkers - amountToRemove, getNumberOfWaitingRequestByRegion(r), getAverageWaitTimeByRegion(r)), maxWaitTime.get(r))) {
                amountToRemove++;
            }
        }

        return amountToRemove;
    }

    private int shouldScaleOutByHowMuch(Region r) {
        int workersToAdd = 0;
        long oldWorkers = getWorkerCountByRegion(r);
        if (shouldScaleOutInitial(calculateExpectedWaitTime(oldWorkers, getNumberOfWaitingRequestByRegion(r), getAverageWaitTimeByRegion(r)), maxWaitTime.get(r))) {
            while (shouldScaleOut(calculateExpectedWaitTime(oldWorkers + workersToAdd, getNumberOfWaitingRequestByRegion(r), getAverageWaitTimeByRegion(r)), maxWaitTime.get(r))) {
                workersToAdd++;
            }
        }

        return workersToAdd;
    }

    private boolean shouldScaleOut(double expectedWaitTime, long maxWaitTime) {
        return expectedWaitTime > maxWaitTime;
    }

    private boolean shouldScaleDown(double expectedWaitTime, long maxWaitTime) {
        return expectedWaitTime < maxWaitTime;
    }

    private boolean shouldScaleOutInitial(double expectedWaitTime, long maxWaitTime) {
        return expectedWaitTime > maxWaitTime * (1 + ALPHA);
    }

    private boolean shouldScaleDownInitial(double expectedWaitTime, long maxWaitTime) {
        return expectedWaitTime < maxWaitTime * (1 - OMEGA);
    }

    private Double getAverageWaitTimeByRegion(Region r) {
        return workloadMonitor.getAverageProcessingTime().get(r);
    }

    private Long getWorkerCountByRegion(Region r) {
        return workloadMonitor.getWorkerCount().get(r);
    }

    private Long getNumberOfWaitingRequestByRegion(Region r) {
        return workloadMonitor.getRequestCount().get(r);
    }

    private Double calculateExpectedWaitTime(long numberOfWorkers, double numberOfWaitingRequests, double averageProcessingTime) {
        return (numberOfWaitingRequests / numberOfWorkers) * averageProcessingTime;
    }
}
