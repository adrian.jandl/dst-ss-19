package dst.ass3.elastic.impl;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.StartContainerCmd;
import com.github.dockerjava.api.exception.NotFoundException;
import com.github.dockerjava.core.DockerClientBuilder;
import dst.ass3.elastic.ContainerException;
import dst.ass3.elastic.ContainerInfo;
import dst.ass3.elastic.ContainerNotFoundException;
import dst.ass3.elastic.IContainerService;
import dst.ass3.messaging.Region;

import java.util.List;
import java.util.stream.Collectors;

public class ContainerServiceImpl implements IContainerService {

    private DockerClient dockerClient;
    private static String IMAGE = "dst/ass3-worker";

    ContainerServiceImpl() {
        dockerClient = DockerClientBuilder.getInstance("tcp://192.168.99.99:2375").build();
    }

    /**
     * Returns a list of all running containers.
     *
     * @return a list of ContainerInfo objects
     * @throws ContainerException if an error occurred when trying to fetch the running containers.
     */
    @Override
    public List<ContainerInfo> listContainers() throws ContainerException {
        return dockerClient.listContainersCmd().exec().stream().map(c -> {
            ContainerInfo info = new ContainerInfo();
            info.setContainerId(c.getId());
            info.setImage(c.getImage());
            info.setWorkerRegion(regionFromCommandString(c.getCommand()));
            info.setRunning(true);
            return info;
        }).collect(Collectors.toList());
    }

    private Region regionFromCommandString(String command) {
        String[] split = command.split(" ");
        return Region.valueOf(split[split.length - 1].toUpperCase());
    }

    /**
     * Stops the container with the given container ID.
     *
     * @param containerId ID of the container to stop.
     * @throws ContainerNotFoundException if the container to stop is not running
     * @throws ContainerException         if another error occurred when trying to stop the container
     */
    @Override
    public void stopContainer(String containerId) throws ContainerException {
        try {
            dockerClient.stopContainerCmd(containerId).exec();
        } catch (NotFoundException e) {
            throw new ContainerNotFoundException("couldnt find container: " + containerId);
        }
    }

    /**
     * Starts a worker for the specific {@link Region}.
     *
     * @param region {@link Region} of the worker to start
     * @return ContainerInfo of the started container / worker
     * @throws ImageNotFoundException if the worker docker image is not available
     * @throws ContainerException     if another error occurred when trying to start the worker
     */
    @Override
    public ContainerInfo startWorker(Region region) throws ContainerException {
        CreateContainerResponse containerResponse = dockerClient
                .createContainerCmd(IMAGE)
                .withCmd(region.toString().toLowerCase())
                .exec();
        StartContainerCmd cmd = dockerClient.startContainerCmd(containerResponse.getId());

        ContainerInfo info = new ContainerInfo();
        info.setWorkerRegion(region);
        info.setRunning(true);
        info.setContainerId(cmd.getContainerId());
        info.setImage(IMAGE);
        cmd.exec();

        return info;
    }
}
