package dst.ass3.messaging.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import dst.ass3.messaging.Constants;
import dst.ass3.messaging.IRequestGateway;
import dst.ass3.messaging.Region;
import dst.ass3.messaging.TripRequest;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RequestGatewayImpl implements IRequestGateway {
    private ObjectMapper objectMapper;
    private ConnectionFactory factory;

    private Connection connection;
    private Channel channel;

    public RequestGatewayImpl() {
        this.objectMapper = new ObjectMapper();
        factory = new ConnectionFactory();
        factory.setHost(Constants.RMQ_HOST);
        factory.setPort(Integer.parseInt(Constants.RMQ_PORT));
        factory.setVirtualHost(Constants.RMQ_VHOST);

        factory.setUsername(Constants.RMQ_USER);
        factory.setPassword(Constants.RMQ_PASSWORD);
        try {
            connection = factory.newConnection();
            channel = connection.createChannel();

            channel.exchangeDeclare(Constants.TOPIC_EXCHANGE, "topic", true);

            channel.queueDeclare(Constants.QUEUE_AT_LINZ, false, false, false, null);
            channel.queueBind(Constants.QUEUE_AT_LINZ, Constants.TOPIC_EXCHANGE, Constants.ROUTING_KEY_AT_LINZ);

            channel.queueDeclare(Constants.QUEUE_DE_BERLIN, false, false, false, null);
            channel.queueBind(Constants.QUEUE_DE_BERLIN, Constants.TOPIC_EXCHANGE, Constants.ROUTING_KEY_DE_BERLIN);

            channel.queueDeclare(Constants.QUEUE_AT_VIENNA, false, false, false, null);
            channel.queueBind(Constants.QUEUE_AT_VIENNA, Constants.TOPIC_EXCHANGE, Constants.ROUTING_KEY_AT_VIENNA);

        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    /**
     * Serializes and routes a request to the correct queue.
     *
     * @param request the request
     */
    @Override
    public void submitRequest(TripRequest request) {
        try {
            String str = objectMapper.writeValueAsString(request);
            submitTripRequest(request.getRegion(), str);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes any resources that may have been initialized (connections, channels, etc.)
     *
     * @throws IOException propagated exceptions
     */
    @Override
    public void close() throws IOException {
        try {
            channel.close();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        connection.close();
    }


    void submitTripRequest(Region region, String json) {
        try {
            switch (region) {
                case AT_VIENNA:
                    channel.basicPublish(Constants.TOPIC_EXCHANGE, Constants.ROUTING_KEY_AT_VIENNA, null, json.getBytes());
                    break;
                case AT_LINZ:
                    channel.basicPublish(Constants.TOPIC_EXCHANGE, Constants.ROUTING_KEY_AT_LINZ, null, json.getBytes());
                    break;
                case DE_BERLIN:
                    channel.basicPublish(Constants.TOPIC_EXCHANGE, Constants.ROUTING_KEY_DE_BERLIN, null, json.getBytes());
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
