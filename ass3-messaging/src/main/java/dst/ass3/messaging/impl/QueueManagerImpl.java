package dst.ass3.messaging.impl;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import dst.ass3.messaging.Constants;
import dst.ass3.messaging.IQueueManager;
import dst.ass3.messaging.Region;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

public class QueueManagerImpl implements IQueueManager {
    private ConnectionFactory factory;

    private Connection connection;
    private Channel channel;

    public QueueManagerImpl() {
        factory = new ConnectionFactory();
        factory.setHost(Constants.RMQ_HOST);
        factory.setPort(Integer.parseInt(Constants.RMQ_PORT));
        factory.setVirtualHost(Constants.RMQ_VHOST);

        factory.setUsername(Constants.RMQ_USER);
        factory.setPassword(Constants.RMQ_PASSWORD);
    }

    /**
     * Initializes all queues or topic exchanges necessary for running the system.
     */
    @Override
    public void setUp() {
        connection = null;
        try {
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.exchangeDeclare(Constants.TOPIC_EXCHANGE, "topic", true);

            channel.queueDeclare(Constants.QUEUE_AT_LINZ, false, false, false, null);
            channel.queueBind(Constants.QUEUE_AT_LINZ, Constants.TOPIC_EXCHANGE, Constants.ROUTING_KEY_AT_LINZ);

            channel.queueDeclare(Constants.QUEUE_DE_BERLIN, false, false, false, null);
            channel.queueBind(Constants.QUEUE_DE_BERLIN, Constants.TOPIC_EXCHANGE, Constants.ROUTING_KEY_DE_BERLIN);

            channel.queueDeclare(Constants.QUEUE_AT_VIENNA, false, false, false, null);
            channel.queueBind(Constants.QUEUE_AT_VIENNA, Constants.TOPIC_EXCHANGE, Constants.ROUTING_KEY_AT_VIENNA);
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes all queues or topic exchanged associated with the system.
     */
    @Override
    public void tearDown() {
        Arrays.stream(Constants.WORK_QUEUES).forEach(name -> {
                    try {
                        channel.queueDelete(name);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        );

        try {
            channel.exchangeDelete(Constants.TOPIC_EXCHANGE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes underlying conection or resources, if any.
     *
     * @throws IOException propagated exceptions
     */
    @Override
    public void close() throws IOException {
        connection.close();
    }
}

