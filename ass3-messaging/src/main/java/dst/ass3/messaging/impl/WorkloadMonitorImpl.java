package dst.ass3.messaging.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.domain.QueueInfo;
import dst.ass3.messaging.Constants;
import dst.ass3.messaging.IWorkloadMonitor;
import dst.ass3.messaging.Region;
import dst.ass3.messaging.WorkerResponse;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class WorkloadMonitorImpl implements IWorkloadMonitor {
    private Client client;
    private Connection connection;
    private Channel channel;
    private ObjectMapper objectMapper;
    private Map<Region, MyConsumer> consumers = new HashMap<>();
    private Map<Region, String> queueNames = new HashMap<>();
    private Map<Region, String> routingKeys = new HashMap<>();

    WorkloadMonitorImpl() {
        //usually would put this setup code in a setup method but for this assignment it's fine in cstr

        objectMapper = new ObjectMapper();

        try {
            this.client = new Client(Constants.RMQ_API_URL, Constants.RMQ_USER, Constants.RMQ_PASSWORD);
        } catch (MalformedURLException | URISyntaxException e) {
            e.printStackTrace();
        }

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(Constants.RMQ_HOST);
        factory.setPort(Integer.parseInt(Constants.RMQ_PORT));
        factory.setVirtualHost(Constants.RMQ_VHOST);

        factory.setUsername(Constants.RMQ_USER);
        factory.setPassword(Constants.RMQ_PASSWORD);

        routingKeys.put(Region.AT_LINZ, Constants.ROUTING_KEY_AT_LINZ);
        routingKeys.put(Region.AT_VIENNA, Constants.ROUTING_KEY_AT_VIENNA);
        routingKeys.put(Region.DE_BERLIN, Constants.ROUTING_KEY_DE_BERLIN);

        try {
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.exchangeDeclare(Constants.TOPIC_EXCHANGE, "topic", true);

            Arrays.stream(Region.values()).forEach(r -> {
                consumers.put(r, new MyConsumer(r));
                try {
                    String queueName = channel.queueDeclare().getQueue();
                    channel.queueBind(queueName, Constants.TOPIC_EXCHANGE, routingKeys.get(r));

                    channel.basicConsume(queueName, true, routingKeys.get(r), consumers.get(r));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns for each region the amount of waiting requests.
     *
     * @return a map
     */
    @Override
    public Map<Region, Long> getRequestCount() {
        Map<Region, Long> map = new HashMap<>();
        getQueueInfoByRegion().forEach(((region, queueInfo) -> map.put(region, queueInfo.getMessagesReady())));

        return map;
    }

    /**
     * Returns the amount of workers for each region. This can be deduced from the amount of consumers to each
     * queue.
     *
     * @return a map
     */
    @Override
    public Map<Region, Long> getWorkerCount() {
        Map<Region, Long> map = new HashMap<>();
        getQueueInfoByRegion().forEach(((region, queueInfo) -> map.put(region, queueInfo.getConsumerCount())));

        return map;
    }

    /**
     * Returns for each region the average processing time of the last 10 recorded requests. The data comes from
     * subscriptions to the respective topics.
     *
     * @return a map
     */
    @Override
    public Map<Region, Double> getAverageProcessingTime() {
        Map<Region, Double> map = new HashMap<>();
        consumers.forEach((region, myConsumer) -> map.put(region, myConsumer.getAverageProcessingTime()));
        return map;
    }

    @Override
    public void close() throws IOException {
        queueNames.values().forEach(s -> {
            try {
                channel.queueDelete(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        try {
            channel.close();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        connection.close();
    }

    private Map<Region, QueueInfo> getQueueInfoByRegion() {

        Map<Region, QueueInfo> map = new HashMap<>();
        map.put(Region.AT_LINZ, client.getQueue(Constants.RMQ_VHOST, Constants.QUEUE_AT_LINZ));
        map.put(Region.AT_VIENNA, client.getQueue(Constants.RMQ_VHOST, Constants.QUEUE_AT_VIENNA));
        map.put(Region.DE_BERLIN, client.getQueue(Constants.RMQ_VHOST, Constants.QUEUE_DE_BERLIN));
        return map;
    }

    private class MyConsumer implements Consumer {
        private Region region;
        private int SIZE = 10;
        private LimitedQueue<Long> limitedQueue = new LimitedQueue<>(SIZE);

        MyConsumer(Region region) {
            this.region = region;
        }

        double getAverageProcessingTime() {
            return (double) limitedQueue.stream().mapToLong(i -> i).sum() / SIZE;
        }

        public Region getRegion() {
            return region;
        }

        @Override
        public void handleConsumeOk(String s) {
            System.out.println("handleConsumeOk");
        }

        @Override
        public void handleCancelOk(String s) {
            System.out.println("handleCancelOk");
        }

        @Override
        public void handleCancel(String s) throws IOException {
            System.out.println("handleCancel");
        }

        @Override
        public void handleShutdownSignal(String s, ShutdownSignalException e) {
            System.out.println("handleShutdownSignal");
        }

        @Override
        public void handleRecoverOk(String s) {
            System.out.println("handleRecoverOk");
        }

        @Override
        public void handleDelivery(String s, Envelope envelope, AMQP.BasicProperties basicProperties, byte[] bytes) throws IOException {
            try {
                String msg = new String(bytes);
                WorkerResponse response = objectMapper.readValue(msg, WorkerResponse.class);
                limitedQueue.add(response.getProcessingTime());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
