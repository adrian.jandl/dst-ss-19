package dst.ass3.messaging.impl;

import java.util.LinkedList;

//taken from StackOverflow https://stackoverflow.com/questions/5498865/size-limited-queue-that-holds-last-n-elements-in-java
//since Guava is not allowed
public class LimitedQueue<E> extends LinkedList<E> {
    private int limit;

    public LimitedQueue(int limit) {
        this.limit = limit;
    }

    @Override
    public boolean add(E o) {
        super.add(o);
        while (size() > limit) { super.remove(); }
        return true;
    }
}

