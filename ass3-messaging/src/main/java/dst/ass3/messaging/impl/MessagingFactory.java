package dst.ass3.messaging.impl;

import dst.ass3.messaging.IMessagingFactory;
import dst.ass3.messaging.IQueueManager;
import dst.ass3.messaging.IRequestGateway;
import dst.ass3.messaging.IWorkloadMonitor;

public class MessagingFactory implements IMessagingFactory {


    @Override
    public IQueueManager createQueueManager() {
        return new QueueManagerImpl();
    }

    @Override
    public IRequestGateway createRequestGateway() {
        return new RequestGatewayImpl();
    }

    @Override
    public IWorkloadMonitor createWorkloadMonitor() {
        return new WorkloadMonitorImpl();
    }

    @Override
    public void close() {

    }
}