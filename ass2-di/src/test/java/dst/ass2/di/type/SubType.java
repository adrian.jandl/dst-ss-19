package dst.ass2.di.type;

import static dst.ass2.di.annotation.Scope.PROTOTYPE;

import dst.ass2.di.annotation.Component;
import dst.ass2.di.annotation.ComponentId;
import dst.ass2.di.annotation.Inject;

@Component(scope = PROTOTYPE)
public class SubType extends SuperType {
    @ComponentId
    private Long id;
    @Inject
    private SimpleComponent component;
}
