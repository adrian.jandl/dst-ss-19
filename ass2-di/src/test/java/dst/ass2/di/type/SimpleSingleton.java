package dst.ass2.di.type;

import static dst.ass2.di.annotation.Scope.SINGLETON;

import dst.ass2.di.annotation.Component;
import dst.ass2.di.annotation.ComponentId;

@Component(scope = SINGLETON)
public class SimpleSingleton {
    @ComponentId
    public Long id;
}
