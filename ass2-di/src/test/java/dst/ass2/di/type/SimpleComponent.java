package dst.ass2.di.type;

import static dst.ass2.di.annotation.Scope.PROTOTYPE;

import dst.ass2.di.annotation.Component;
import dst.ass2.di.annotation.ComponentId;

@Component(scope = PROTOTYPE)
public class SimpleComponent {
    @ComponentId
    public Long id;
}
