package dst.ass2.di.type;

import static dst.ass2.di.annotation.Scope.PROTOTYPE;

import dst.ass2.di.annotation.Component;
import dst.ass2.di.annotation.ComponentId;
import dst.ass2.di.annotation.Inject;

@Component(scope = PROTOTYPE)
public class Container {
    @ComponentId
    public Long id;
    public Long timestamp = System.currentTimeMillis();
    @Inject
    public SimpleSingleton first;
    @Inject
    public SimpleSingleton second;
    @Inject
    public SimpleComponent component;
    @Inject
    public SimpleComponent anotherComponent;
}
