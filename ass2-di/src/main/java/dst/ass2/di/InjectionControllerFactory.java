package dst.ass2.di;

/**
 * Creates and provides {@link IInjectionController} instances.
 */
public class InjectionControllerFactory {

    private static IInjectionController STANDALONE_INSTANCE;
    private static IInjectionController TRANSPARENT_INSTANCE;

    /**
     * Returns the singleton {@link IInjectionController} instance.<br/>
     * If none is available, a new one is created.
     *
     * @return the instance
     */
    public static synchronized IInjectionController getStandAloneInstance() {
        if (STANDALONE_INSTANCE == null) {
            STANDALONE_INSTANCE = getNewStandaloneInstance();
        }
        return STANDALONE_INSTANCE;
    }

    /**
     * Returns the singleton {@link IInjectionController} instance for processing objects modified by an
     * {@link dst.ass2.di.agent.InjectorAgent InjectorAgent}.<br/>
     * If none is available, a new one is created.
     *
     * @return the instance
     */
    public static synchronized IInjectionController getTransparentInstance() {
        if (TRANSPARENT_INSTANCE == null) {
            TRANSPARENT_INSTANCE = getNewTransparentInstance();
        }
        return TRANSPARENT_INSTANCE;
    }

    /**
     * Creates and returns a new {@link IInjectionController} instance.
     *
     * @return the newly created instance
     */
    public static IInjectionController getNewStandaloneInstance() {
        return new SimpleInjectionController();
    }

    /**
     * Creates and returns a new {@link IInjectionController} instance for processing objects modified by an
     * {@link dst.ass2.di.agent.InjectorAgent InjectorAgent}.<br/>
     *
     * @return the instance
     */
    public static IInjectionController getNewTransparentInstance() {
        return new TransparentInjectionController();
    }
}
