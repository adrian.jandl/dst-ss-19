package dst.ass2.di;

import dst.ass2.di.annotation.Component;
import dst.ass2.di.annotation.ComponentId;
import dst.ass2.di.annotation.Inject;
import dst.ass2.di.annotation.Scope;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.*;

public class TransparentInjectionController implements IInjectionController {
    private Long ID = 0L;
    private Map<Class, Object> singletons = new HashMap<>();

    public TransparentInjectionController() {
        ID = 0L;
    }

    /**
     * Injects all fields annotated with {@link Inject @Inject}.<br/>
     * Only objects of classes annotated with {@link Component @Component} shall be accepted.
     *
     * @param obj the object to initialize.
     * @throws InjectionException if it's no component or dependency injection fails.
     */
    @Override
    public void initialize(Object obj) throws InjectionException {
        if (singletons.containsKey(obj.getClass())) {
//            obj = singletons.get(obj.getClass());
            throw new InjectionException("Cannot initialize Singleton twice");
        }
        Component componentAnnotation = obj.getClass().getAnnotation(Component.class);
        if (componentAnnotation == null) {
            throw new InjectionException("No component");
        }
        System.out.println("In InjectionController");
        List<Field> allFields = new ArrayList<>();
        Class superClass = obj.getClass().getSuperclass();
        while (superClass != null) {
            allFields.addAll(Arrays.asList(obj.getClass().getSuperclass().getDeclaredFields()));
            superClass = superClass.getSuperclass();
        }
        Field[] fields = obj.getClass().getDeclaredFields();

        allFields.addAll(Arrays.asList(fields));
        int amountIds = 0;
        long idValue = ID++;
        for (Field f : allFields) {
            ComponentId id = f.getAnnotation(ComponentId.class);
            if (id != null) {
                amountIds++;
                f.setAccessible(true);
                try {
                    f.set(obj, idValue);
                } catch (IllegalAccessException e) {
                    throw new InjectionException("Failure to inject");
                }
            }

            Inject fieldAnnotation = f.getAnnotation(Inject.class);
            if (fieldAnnotation != null) {
                Class memberClass;
                if (fieldAnnotation.specificType() != Void.class) {
                    memberClass = fieldAnnotation.specificType();
                } else {
                    Type t = f.getType();
                    memberClass = ((Class) t);
                }
                try {
                    f.setAccessible(true);
                    Component memberComponentAnnotation = (Component) memberClass.getAnnotation(Component.class);
                    if (memberComponentAnnotation != null) {
                        if (memberComponentAnnotation.scope() == Scope.SINGLETON) {
                            f.set(obj, getSingletonInstance(memberClass));
                        } else {
                            Object memberObject = memberClass.newInstance();
                            f.set(obj, memberObject);
                        }
                    }
                } catch (InstantiationException | IllegalAccessException | IllegalArgumentException e) {
                    if (fieldAnnotation.required()) {
                        e.printStackTrace();
                        throw new InjectionException("Could not instantiate field");
                    }
                }
            }
        }
        if (amountIds == 0) {
            throw new InjectionException("No id fields found");
        }
        if (componentAnnotation.scope() == Scope.SINGLETON) {
            singletons.put(obj.getClass(), obj);
        }
    }

    /**
     * Gets the fully initialized instance of a singleton component.<br/>
     * Multiple calls of this method always have to return the same instance.
     * Only classes that are annotated with {@link Component Component} shall be accepted.
     *
     * @param clazz the class of the component.
     * @return the initialized singleton object.
     * @throws InjectionException if it's no component or dependency injection fails.
     */
    @Override
    public <T> T getSingletonInstance(Class<T> clazz) throws InjectionException {
        Component componentAnnotation = clazz.getAnnotation(Component.class);
        if (componentAnnotation != null && componentAnnotation.scope() != Scope.SINGLETON) {
            throw new InjectionException("Cannot construct singleton of class with scope " + componentAnnotation.scope());
        }
        try {
            if (singletons.containsKey(clazz)) {
                return (T) singletons.get(clazz);
            } else {
                T obj = clazz.newInstance();
                singletons.put(clazz, obj);
                return obj;
            }
        } catch (InstantiationException | IllegalAccessException e) {
            throw new InjectionException("could not inject");
        }
    }
}