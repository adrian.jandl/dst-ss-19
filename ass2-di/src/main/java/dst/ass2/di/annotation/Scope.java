package dst.ass2.di.annotation;

public enum Scope {

    SINGLETON, PROTOTYPE

}
