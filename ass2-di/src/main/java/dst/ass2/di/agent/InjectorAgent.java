package dst.ass2.di.agent;

import dst.ass2.di.annotation.Component;
import javassist.*;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;

public class InjectorAgent implements ClassFileTransformer {

    public static void premain(String agentArgs, Instrumentation inst) {
        inst.addTransformer(new InjectorAgent());
    }

    @Override
    public byte[] transform(ClassLoader loader, String className,
                            Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {
        try {

            System.out.println(className);
            ClassPool cp = ClassPool.getDefault();
            CtClass ct = cp.get(className.replace("/", "."));
            cp.importPackage("dst.ass2.di");

            Object c = ct.getAnnotation(Component.class);

            if (c == null) {
                return classfileBuffer;
            } else {
                System.out.println(c);

                CtConstructor cstr = ct.getDeclaredConstructor(new CtClass[]{});
                StringBuilder sb = new StringBuilder();
                sb.append("dst.ass2.di.IInjectionController ic = dst.ass2.di.InjectionControllerFactory.getTransparentInstance();");
//                cstr.insertBeforeBody();
                CtField[] fields = ct.getDeclaredFields();
                //cstr.insertBefore("ic ");
//                cstr.insertBefore("dst.ass2.di.IInjectionController ic = new dst.ass2.di.SimpleInjectionController();");
             /*   boolean found = false;
                for (int i = 0; i < fields.length; i++) {
                    CtField field = fields[i];
                    if (field.getAnnotation(Inject.class) != null) {
                        //cstr.insertBeforeBody(field.getName() + "=ic.initialize(" + field.getName() + ");");
                        System.out.println(field.getName());
                        if (!found) {
                            sb.append(field.getName()).append("=ic.initialize(").append(field.getName()).append(");");
                            found = true;
                        }

                    }
                }*/
                sb.append("ic.initialize(this);");
                cstr.insertBeforeBody(sb.toString());
                return ct.toBytecode();
            }
        } catch (NotFoundException | CannotCompileException | ClassNotFoundException | IOException e) {
            //System.out.println(className);
            e.printStackTrace();
            return classfileBuffer;
        }
        // TODO
    }

}
