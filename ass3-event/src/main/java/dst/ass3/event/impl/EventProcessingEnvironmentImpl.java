package dst.ass3.event.impl;

import dst.ass3.event.IEventProcessingEnvironment;
import dst.ass3.event.model.domain.ITripEventInfo;
import dst.ass3.event.model.domain.TripState;
import dst.ass3.event.model.events.*;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.cep.CEP;
import org.apache.flink.cep.PatternStream;
import org.apache.flink.cep.functions.PatternProcessFunction;
import org.apache.flink.cep.functions.TimedOutPartialMatchHandler;
import org.apache.flink.cep.nfa.aftermatch.AfterMatchSkipStrategy;
import org.apache.flink.cep.pattern.Pattern;
import org.apache.flink.cep.pattern.conditions.SimpleCondition;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.assigners.GlobalWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.CountTrigger;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EventProcessingEnvironmentImpl implements IEventProcessingEnvironment {

    private Time matchingTimeout;

    private SinkFunction<LifecycleEvent> lifecycleEventSinkFunction;
    private SinkFunction<MatchingDuration> matchingDurationSinkFunction;
    private SinkFunction<MatchingTimeoutWarning> matchingTimeoutWarningSinkFunction;
    private SinkFunction<TripFailedWarning> tripFailedWarningStreamSink;
    private SinkFunction<Alert> alertStreamSink;
    private SinkFunction<AverageMatchingDuration> averageMatchingDurationStreamSink;

    /**
     * Initializes the event processing graph on the {@link StreamExecutionEnvironment}. This function is called
     * after all sinks have been set.
     *
     * @param env
     */
    @Override
    public void initialize(StreamExecutionEnvironment env) {

        DataStream<ITripEventInfo> tripEventStream = env.addSource(new EventSourceFunctionImpl());
        DataStream<LifecycleEvent> lifecycleEventDataStream = tripEventStream.
                filter(iTripEventInfo -> iTripEventInfo.getRegion() != null)
                .map(LifecycleEvent::new)
                .assignTimestampsAndWatermarks(new AssignerWithPunctuatedWatermarks<LifecycleEvent>() {
                    @Override
                    public Watermark checkAndGetNextWatermark(LifecycleEvent lifecycleEvent, long l) {
                        return new Watermark(l);
                    }

                    @Override
                    public long extractTimestamp(LifecycleEvent lifecycleEvent, long l) {
                        return lifecycleEvent.getTimestamp();
                    }
                });

        lifecycleEventDataStream.addSink(lifecycleEventSinkFunction);

        Pattern<LifecycleEvent, ?> pattern = Pattern
                .<LifecycleEvent>begin("start")
                .where(new SimpleCondition<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) {
                        return lifecycleEvent.getState() == TripState.CREATED;
                    }
                })
                .followedBy("end")
                .where(new SimpleCondition<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) {
                        return lifecycleEvent.getState() == TripState.MATCHED;
                    }
                })
                .within(matchingTimeout);


        PatternStream<LifecycleEvent> patternStream = CEP.pattern(lifecycleEventDataStream.keyBy("tripId"), pattern);

        SingleOutputStreamOperator<MatchingDuration> matchingDurationDataStream = patternStream.process(new MyPatternProcessFunction());
        matchingDurationDataStream.addSink(matchingDurationSinkFunction);


        Pattern<LifecycleEvent, ?> tripFailedPattern = Pattern
                .<LifecycleEvent>begin("startTripFailed", AfterMatchSkipStrategy.skipPastLastEvent())
                .next("middle")
                .where(new SimpleCondition<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) {
                        return lifecycleEvent.getState() == TripState.MATCHED;
                    }
                })
                .next("endTripFailed")
                .where(new SimpleCondition<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) {
                        return lifecycleEvent.getState() == TripState.QUEUED;
                    }
                })
                .timesOrMore(3);

        SingleOutputStreamOperator<TripFailedWarning> tripFailedWarningSingleOutputStreamOperator = CEP
                .pattern(lifecycleEventDataStream.keyBy("tripId"), tripFailedPattern)
                .process(new MyTripFailedPatternProcessFunction());
        tripFailedWarningSingleOutputStreamOperator.addSink(tripFailedWarningStreamSink);


        DataStream<AverageMatchingDuration> averageMatchingDurationDataStream = matchingDurationDataStream
                .keyBy("region")
                .window(GlobalWindows.create())
                .trigger(CountTrigger.of(5))
                .process(
                        new ProcessWindowFunction<MatchingDuration, AverageMatchingDuration, Tuple, GlobalWindow>() {
                            @Override
                            public void process(Tuple tuple, Context context, Iterable<MatchingDuration> iterable, Collector<AverageMatchingDuration> collector) throws Exception {
                                AverageMatchingDuration averageMatchingDuration = new AverageMatchingDuration();

                                long duration = 0L;
                                for (MatchingDuration d : iterable) {
                                    duration += d.getDuration();
                                    averageMatchingDuration.setRegion(d.getRegion());
                                }
                                averageMatchingDuration.setDuration(duration / 5.0);

                                collector.collect(averageMatchingDuration);
                            }
                        }
                );

        averageMatchingDurationDataStream.addSink(averageMatchingDurationStreamSink);

        DataStream<MatchingTimeoutWarning> timeoutWarningDataStream = matchingDurationDataStream.getSideOutput(new OutputTag<MatchingTimeoutWarning>("matchingTimeoutWarnings") {
        });
        timeoutWarningDataStream.addSink(matchingTimeoutWarningSinkFunction);

        SingleOutputStreamOperator<Alert> alertSingleOutputStreamOperator = tripFailedWarningSingleOutputStreamOperator
                .map(new MapFunction<TripFailedWarning, Warning>() {
                    @Override
                    public Warning map(TripFailedWarning tripFailedWarning) {
                        return tripFailedWarning;
                    }
                }).union(timeoutWarningDataStream.map(new MapFunction<MatchingTimeoutWarning, Warning>() {
                    @Override
                    public Warning map(MatchingTimeoutWarning matchingTimeoutWarning) {
                        return matchingTimeoutWarning;
                    }
                }))
                .keyBy("region")
                .window(GlobalWindows.create())
                .trigger(CountTrigger.of(3))
                .process(new ProcessWindowFunction<Warning, Alert, Tuple, GlobalWindow>() {
                    @Override
                    public void process(Tuple tuple, Context context, Iterable<Warning> iterable, Collector<Alert> collector) {
                        List<Warning> warnings = new ArrayList<>();
                        iterable.forEach(warnings::add);
                        Alert alert = new Alert(warnings.get(0).getRegion(), warnings);
                        collector.collect(alert);
                    }
                });

        alertSingleOutputStreamOperator.addSink(alertStreamSink);

        System.out.println(env.getExecutionPlan());
    }

    /**
     * Sets the timeout limit of a streaming event.
     *
     * @param time the timeout limit
     */
    @Override
    public void setMatchingDurationTimeout(Time time) {
        this.matchingTimeout = time;
    }

    @Override
    public void setLifecycleEventStreamSink(SinkFunction<LifecycleEvent> sink) {
        lifecycleEventSinkFunction = sink;
    }

    @Override
    public void setMatchingDurationStreamSink(SinkFunction<MatchingDuration> sink) {
        matchingDurationSinkFunction = sink;
    }

    @Override
    public void setAverageMatchingDurationStreamSink(SinkFunction<AverageMatchingDuration> sink) {
        averageMatchingDurationStreamSink = sink;
    }

    @Override
    public void setMatchingTimeoutWarningStreamSink(SinkFunction<MatchingTimeoutWarning> sink) {
        matchingTimeoutWarningSinkFunction = sink;
    }

    @Override
    public void setTripFailedWarningStreamSink(SinkFunction<TripFailedWarning> sink) {
        tripFailedWarningStreamSink = sink;
    }

    @Override
    public void setAlertStreamSink(SinkFunction<Alert> sink) {
        alertStreamSink = sink;
    }

    private class MyPatternProcessFunction extends PatternProcessFunction<LifecycleEvent, MatchingDuration> implements TimedOutPartialMatchHandler<LifecycleEvent> {
        @Override
        public void processMatch(Map<String, List<LifecycleEvent>> map, Context context, Collector<MatchingDuration> collector) throws Exception {
            LifecycleEvent end = map.get("end").get(0);
            LifecycleEvent start = map.get("start").get(0);

            collector.collect(new MatchingDuration(end.getTripId(), end.getRegion(), end.getTimestamp() - start.getTimestamp()));
        }

        private OutputTag<MatchingTimeoutWarning> outputTag = new OutputTag<MatchingTimeoutWarning>("matchingTimeoutWarnings") {
        };

        @Override
        public void processTimedOutMatch(Map<String, List<LifecycleEvent>> map, Context context) throws Exception {
            List<LifecycleEvent> list = new ArrayList<>();
            for (Map.Entry<String, List<LifecycleEvent>> entry : map.entrySet()) {
                List<LifecycleEvent> lifecycleEvents = entry.getValue();
                list.addAll(lifecycleEvents);
            }
            list.forEach(lifecycleEvent -> context.output(outputTag, new MatchingTimeoutWarning(lifecycleEvent.getTripId(), lifecycleEvent.getRegion())));
        }
    }

    private class MyTripFailedPatternProcessFunction extends PatternProcessFunction<LifecycleEvent, TripFailedWarning> {

        @Override
        public void processMatch(Map<String, List<LifecycleEvent>> map, Context context, Collector<TripFailedWarning> collector) throws Exception {
            LifecycleEvent e = map.get("endTripFailed").get(0);
            collector.collect(new TripFailedWarning(e.getTripId(), e.getRegion()));
        }
    }


    // I tried it with an aggregate together with countWindow(5) but couldn't get it to work
    private static class AverageAggregate
            implements AggregateFunction<MatchingDuration, Tuple2<Long, Long>, AverageMatchingDuration> {

        @Override
        public Tuple2<Long, Long> createAccumulator() {
            return new Tuple2<>(0L, 0L);
        }

        @Override
        public Tuple2<Long, Long> add(MatchingDuration matchingDuration, Tuple2<Long, Long> longLongTuple2) {
            return new Tuple2<>(longLongTuple2.f0 + matchingDuration.getDuration(), longLongTuple2.f1 + 1);
        }

        @Override
        public AverageMatchingDuration getResult(Tuple2<Long, Long> longLongTuple2) {
            AverageMatchingDuration duration = new AverageMatchingDuration();
            duration.setDuration(longLongTuple2.f0 / longLongTuple2.f1);

            return duration;
        }

        @Override
        public Tuple2<Long, Long> merge(Tuple2<Long, Long> longLongTuple2, Tuple2<Long, Long> acc1) {
            return new Tuple2<>(longLongTuple2.f0 + acc1.f0, longLongTuple2.f1 + acc1.f1);
        }
    }

}
