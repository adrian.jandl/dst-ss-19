package dst.ass3.event.impl;

import dst.ass3.event.Constants;
import dst.ass3.event.EventSubscriber;
import dst.ass3.event.IEventSourceFunction;
import dst.ass3.event.model.domain.ITripEventInfo;
import org.apache.flink.api.common.functions.IterationRuntimeContext;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.configuration.Configuration;

import java.net.InetSocketAddress;

public class EventSourceFunctionImpl implements IEventSourceFunction {
    EventSubscriber eventSubscriber;

    @Override
    public void open(Configuration parameters) throws Exception {
        eventSubscriber = EventSubscriber.subscribe(new InetSocketAddress(Constants.EVENT_PUBLISHER_PORT));
    }

    @Override
    public void close() throws Exception {
        eventSubscriber.close();
    }

    @Override
    public RuntimeContext getRuntimeContext() {
        return null;
    }

    @Override
    public IterationRuntimeContext getIterationRuntimeContext() {
        return null;
    }

    @Override
    public void setRuntimeContext(RuntimeContext runtimeContext) {

    }

    @Override
    public void run(SourceContext<ITripEventInfo> ctx) throws Exception {
        ITripEventInfo info;
        info = eventSubscriber.receive();
        while (info != null) {
            ctx.collect(info);
            info = eventSubscriber.receive();
        }
    }

    @Override
    public void cancel() {

    }
}
