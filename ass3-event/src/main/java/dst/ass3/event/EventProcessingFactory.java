package dst.ass3.event;

import dst.ass3.event.impl.EventProcessingEnvironmentImpl;
import dst.ass3.event.impl.EventSourceFunctionImpl;

/**
 * Creates your {@link IEventProcessingEnvironment} and {@link IEventSourceFunction} implementation instances.
 */
public class EventProcessingFactory {
    public static IEventProcessingEnvironment createEventProcessingEnvironment() {
        return new EventProcessingEnvironmentImpl();
    }

    public static IEventSourceFunction createEventSourceFunction() {
        return new EventSourceFunctionImpl();
    }
}
