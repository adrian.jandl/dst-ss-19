package dst.ass1.jpa.listener;


import javax.persistence.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class DefaultListener {

    private static AtomicInteger loadOperations = new AtomicInteger(0);
    private static AtomicInteger updateOperations = new AtomicInteger(0);
    private static AtomicInteger removeOperations = new AtomicInteger(0);
    private static AtomicInteger persistOperations = new AtomicInteger(0);
    private static AtomicLong persistTime = new AtomicLong(0);
    private static ThreadLocal<Long> start = new ThreadLocal<>();

    public static int getLoadOperations() {
        return loadOperations.intValue();
    }

    public static int getUpdateOperations() {
        return updateOperations.intValue();
    }

    public static int getRemoveOperations() {
        return removeOperations.intValue();
    }

    public static int getPersistOperations() {
        return persistOperations.intValue();
    }

    public static long getOverallTimeToPersist() {
        return persistTime.longValue();
    }

    public static double getAverageTimeToPersist() {
        return persistTime.doubleValue() / persistOperations.doubleValue();
    }

    /**
     * Clears the internal data structures that are used for storing the operations.
     */
    public static void clear() {
        loadOperations = new AtomicInteger(0);
        updateOperations = new AtomicInteger(0);
        removeOperations = new AtomicInteger(0);
        persistOperations = new AtomicInteger(0);
        persistTime = new AtomicLong(0);
    }

    @PrePersist
    void prePersist(Object object) {
        start.set(System.currentTimeMillis());
    }

    @PostLoad
    void postLoad(Object object) {
        loadOperations.incrementAndGet();
    }

    @PostRemove
    void postRemove(Object object) {
        removeOperations.incrementAndGet();
    }

    @PostUpdate
    void postUpdate(Object object) {
        updateOperations.incrementAndGet();
    }

    @PostPersist
    void postPersist(Object object) {
        final long end = System.currentTimeMillis();
        final long startLong = start.get();
        persistTime.addAndGet(end - startLong);
        persistOperations.incrementAndGet();
    }
}
