package dst.ass1.jpa.listener;

import dst.ass1.jpa.model.impl.Trip;

import javax.persistence.*;
import java.util.Date;

public class TripListener {
    @PrePersist
    void prePersist(Trip object) {
        object.setCreated(new Date());
        object.setUpdated(new Date());
    }

    @PreUpdate
    void preUpdate(Trip object) {
        object.setUpdated(new Date());
    }

    @PreRemove
    void preRemove(Trip object) {
    }

    @PostLoad
    void postLoad(Trip object) {
    }

    @PostRemove
    void postRemove(Trip object) {
    }

    @PostUpdate
    void postUpdate(Trip object) {
    }

    @PostPersist
    void postPersist(Trip object) {
    }
}
