package dst.ass1.jpa.interceptor;

import org.hibernate.EmptyInterceptor;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

public class SQLInterceptor extends EmptyInterceptor {

    private static final long serialVersionUID = -3082243834965597947L;
    private static AtomicLong selectCount = new AtomicLong(0);
    private static AtomicBoolean verbose = new AtomicBoolean(false);
    private final String regex = "select .* from (trip|location)";
    private final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);


    public static void resetCounter() {
        selectCount.set(0);
    }

    public static int getSelectCount() {
        return selectCount.intValue();
    }

    /**
     * If the verbose argument is set, the interceptor prints the intercepted SQL statements to System.out.
     *
     * @param verbose whether or not to be verbose
     */
    public static void setVerbose(boolean verbose) {
        SQLInterceptor.verbose.set(verbose);
    }

    @Override
    public String onPrepareStatement(String sql) {
        if (pattern.matcher(sql.toLowerCase()).find()) {
            selectCount.incrementAndGet();
        }

        if (verbose.get()) {
            System.out.println(sql);
        }

        return sql;
    }

}
