package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.ITripDAO;
import dst.ass1.jpa.model.ITrip;
import dst.ass1.jpa.model.TripState;
import dst.ass1.jpa.model.impl.Trip;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class TripDAO extends GenericDAOImpl<ITrip> implements ITripDAO {

    public TripDAO(EntityManager em) {
        super(em, Trip.class);
    }

    @Override
    public Collection<ITrip> findCancelledTrips(Date start, Date end) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Trip> q = cb.createQuery(Trip.class);

        Root<Trip> tripRoot = q.from(Trip.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(tripRoot.get("state"), TripState.CANCELLED));
        if (start != null) {
            predicates.add(cb.greaterThan(tripRoot.get("created"), start));
        }
        if (end != null) {
            predicates.add(cb.lessThan(tripRoot.get("created"), end));
        }
        TypedQuery<Trip> typedQuery = em.createQuery(q
                .select(tripRoot)
                .where(predicates.toArray(new Predicate[]{}))
        );

        return new ArrayList<>(typedQuery.getResultList());
    }
}
