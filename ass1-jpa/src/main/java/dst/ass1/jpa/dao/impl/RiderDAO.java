package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IRiderDAO;
import dst.ass1.jpa.model.IMoney;
import dst.ass1.jpa.model.IRider;
import dst.ass1.jpa.model.TripState;
import dst.ass1.jpa.model.impl.Money;
import dst.ass1.jpa.model.impl.Rider;
import dst.ass1.jpa.model.impl.Trip;
import dst.ass1.jpa.model.impl.TripInfo;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.*;

public class RiderDAO extends GenericDAOImpl<IRider> implements IRiderDAO {

    public RiderDAO(EntityManager em) {
        super(em, Rider.class);

    }

    @Override
    public IRider findByEmail(String email) {
        try {
            return (Rider) em
                    .createNamedQuery("riderByEmail")
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            //swallow and return null
            return null;
        }
    }

    @Override
    public Double getTotalDistanceOfMostRecentRider() {
        return (Double) em.createNamedQuery("sumDistanceOfRiderWithMostRecentTrip").getSingleResult();
    }

    @Override
    public Map<IRider, Map<String, IMoney>> getRecentSpending() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> q = cb.createTupleQuery();

        Root<Rider> riderRoot = q.from(Rider.class);
        Join<Rider, Trip> riderTrip = riderRoot.join("trips");
        Join<Trip, TripInfo> tripTripInfo = riderTrip.join("tripInfo");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -30);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        Date d = new Date(c.toInstant().getEpochSecond() * 1000);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.greaterThanOrEqualTo(tripTripInfo.get("completed"), d));
        predicates.add(cb.equal(riderTrip.get("state"), TripState.COMPLETED));
        TypedQuery<Tuple> typedQuery = em.createQuery(q
                .multiselect(riderRoot, tripTripInfo.get("total").get("currency"), cb.sum(tripTripInfo.get("total").get("value")))
                .where(predicates.toArray(new Predicate[]{}))
                .groupBy(tripTripInfo.get("total").get("currency"), riderRoot.get("id"))
        );

        List<Tuple> riders = typedQuery.getResultList();
        Map<IRider, Map<String, IMoney>> map = new HashMap<>();
        for (Tuple t : riders) {
            if (!map.containsKey((Rider) t.get(0))) {
                map.put((Rider) t.get(0), new HashMap<>());
            }
            Map<String, IMoney> x = map.get((Rider) t.get(0));
            if (x.containsKey((String) t.get(1))) {
                BigDecimal current = x.get((String) t.get(1)).getValue();
                x.get((String) t.get(1)).setValue(current.add((BigDecimal) t.get(2)));
            } else {
                Money m = new Money();
                m.setCurrency((String) t.get(1));
                m.setValue((BigDecimal) t.get(2));
                x.put((String) t.get(1), m);
            }

        }
        return map;
    }
}
