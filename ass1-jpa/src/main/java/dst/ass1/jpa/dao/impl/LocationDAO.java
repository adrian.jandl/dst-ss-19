package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.ILocationDAO;
import dst.ass1.jpa.model.ILocation;
import dst.ass1.jpa.model.impl.Location;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class LocationDAO extends GenericDAOImpl<ILocation> implements ILocationDAO {

    public LocationDAO(EntityManager em) {
        super(em, Location.class);
    }

    @Override
    public Collection<Long> findReachedLocationIds() {
        return (List<Long>) em.createNamedQuery("reachedLocations").getResultList();
    }
}
