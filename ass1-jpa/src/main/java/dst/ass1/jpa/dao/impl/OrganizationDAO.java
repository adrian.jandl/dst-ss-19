package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IOrganizationDAO;
import dst.ass1.jpa.model.IOrganization;
import dst.ass1.jpa.model.impl.Organization;

import javax.persistence.EntityManager;

public class OrganizationDAO extends GenericDAOImpl<IOrganization> implements IOrganizationDAO {

    public OrganizationDAO(EntityManager em) {
        super(em, Organization.class);
    }
}
