package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IDriverDAO;
import dst.ass1.jpa.model.IDriver;
import dst.ass1.jpa.model.impl.Driver;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class DriverDAO extends GenericDAOImpl<IDriver> implements IDriverDAO {

    public DriverDAO(EntityManager em) {
        super(em, Driver.class);
    }

    @Override
    public Collection<IDriver> findActiveInMultipleOrganizationsDrivers(Long numberOfOrganizations) {
        return (List<IDriver>) em
                .createNamedQuery("activeInMultipleOrganizationsDrivers")
                .setParameter("organizations", numberOfOrganizations)
                .getResultList();
    }

}
