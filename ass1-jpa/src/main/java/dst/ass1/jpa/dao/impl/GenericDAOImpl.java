package dst.ass1.jpa.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public abstract class GenericDAOImpl<T> implements dst.ass1.jpa.dao.GenericDAO<T> {

    protected EntityManager em;
    private Class clazz;

    public GenericDAOImpl(EntityManager em, Class clazz) {
        this.em = em;
        this.clazz = clazz;
    }

    @Override
    public T findById(Long id) {
        return (T) em.find(clazz, id);
    }

    @Override
    public List<T> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(clazz);
        Root<T> rootEntry = cq.from(clazz);
        CriteriaQuery<T> all = cq.select(rootEntry);
        TypedQuery<T> allQuery = em.createQuery(all);
        List<T> result = allQuery.getResultList();
        if (result == null) {
            return new ArrayList<T>();
        }
        return result;
    }
}
