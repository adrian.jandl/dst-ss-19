package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.listener.DefaultListener;
import dst.ass1.jpa.model.IPreferences;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@EntityListeners(DefaultListener.class)
public class Preferences implements IPreferences {
    @Id
    @GeneratedValue
    private Long id;
    @ElementCollection
    private Map<String, String> data = new HashMap<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Map<String, String> getData() {
        return data;
    }

    @Override
    public void setData(Map<String, String> data) {
        this.data = data;
    }

    @Override
    public void putData(String key, String value) {
        if (this.data == null) {
            data = new HashMap<>();
        }
        data.put(key, value);
    }
}
