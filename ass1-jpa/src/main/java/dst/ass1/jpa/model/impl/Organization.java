package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.listener.DefaultListener;
import dst.ass1.jpa.model.IEmployment;
import dst.ass1.jpa.model.IOrganization;
import dst.ass1.jpa.model.IVehicle;
import dst.ass1.jpa.util.Constants;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@EntityListeners(DefaultListener.class)
public class Organization implements IOrganization {
    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @ManyToMany(targetEntity = Organization.class, cascade = CascadeType.ALL)
    @JoinTable(name = Constants.J_ORGANIZATION_PARTS,
            joinColumns = {@JoinColumn(name = Constants.I_ORGANIZATION_PART_OF)},
            inverseJoinColumns = {@JoinColumn(name = Constants.I_ORGANIZATION_PARTS)})
    private Collection<IOrganization> partOf = new ArrayList<>();

    @ManyToMany(targetEntity = Organization.class, mappedBy = "partOf", cascade = CascadeType.ALL)
    private Collection<IOrganization> parts = new ArrayList<>();

    @OneToMany(targetEntity = Employment.class)
    private Collection<IEmployment> employments = new ArrayList<>();

    @OneToMany(targetEntity = Vehicle.class)
    private Collection<IVehicle> vehicles = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Collection<IOrganization> getPartOf() {
        return partOf;
    }

    @Override
    public void setPartOf(Collection<IOrganization> partOf) {
        this.partOf = partOf;
    }

    @Override
    public void addPartOf(IOrganization partOf) {
        if (this.partOf == null) {
            this.partOf = new ArrayList<>();
        }
        this.partOf.add(partOf);
    }

    @Override
    public Collection<IEmployment> getEmployments() {
        return employments;
    }

    @Override
    public void setEmployments(Collection<IEmployment> employments) {
        this.employments = employments;
    }

    @Override
    public void addEmployment(IEmployment employment) {
        if (this.employments == null) {
            this.employments = new ArrayList<>();
        }
        this.employments.add(employment);
    }

    @Override
    public Collection<IVehicle> getVehicles() {
        return vehicles;
    }

    @Override
    public void setVehicles(Collection<IVehicle> vehicles) {
        this.vehicles = vehicles;
    }

    @Override
    public void addVehicle(IVehicle vehicle) {
        if (this.vehicles == null) {
            this.vehicles = new ArrayList<>();
        }
        this.vehicles.add(vehicle);
    }

    @Override
    public Collection<IOrganization> getParts() {
        return parts;
    }

    @Override
    public void setParts(Collection<IOrganization> parts) {
        this.parts = parts;
    }

    @Override
    public void addPart(IOrganization part) {
        if (parts == null) {
            parts = new ArrayList<>();
        }
        parts.add(part);
    }
}
