package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.listener.DefaultListener;
import dst.ass1.jpa.model.IPreferences;
import dst.ass1.jpa.model.IRider;
import dst.ass1.jpa.model.ITrip;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"accountNo", "bankCode"})})
@NamedQuery(name = "riderByEmail", query = "SELECT r from Rider r WHERE r.email = :email")
@NamedQuery(name = "sumDistanceOfRiderWithMostRecentTrip",
        query = "SELECT sum(ti.distance) from Rider r " +
                "join r.trips t " +
                "join t.tripInfo ti " +
                "where r.id = (select ri.id from Rider ri join ri.trips rit join rit.tripInfo riti where riti.completed = (select max(tripInfo.completed) from TripInfo tripInfo))")
@EntityListeners(DefaultListener.class)
public class Rider extends PlatformUser implements IRider {
    @Column(nullable = false, unique = true)
    private String email;
    @Column(length = 20)
    private byte[] password;
    private String accountNo;
    private String bankCode;
    @OneToOne(targetEntity = Preferences.class, optional = false, cascade = CascadeType.ALL)
    private IPreferences preferences;
    @OneToMany(targetEntity = Trip.class, cascade = CascadeType.ALL, mappedBy = "rider")
    private Collection<ITrip> trips = new HashSet<>();

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public byte[] getPassword() {
        return password;
    }

    @Override
    public void setPassword(byte[] password) {
        this.password = password;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    @Override
    public String getBankCode() {
        return bankCode;
    }

    @Override
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Override
    public IPreferences getPreferences() {
        return preferences;
    }

    @Override
    public void setPreferences(IPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public Collection<ITrip> getTrips() {
        return new HashSet<>(trips);
    }

    @Override
    public void setTrips(Collection<ITrip> trips) {
        this.trips = new CopyOnWriteArrayList<>(trips);
    }

    @Override
    public void addTrip(ITrip trip) {
        if (this.trips == null) {
            this.trips = new CopyOnWriteArrayList<>();
        }
        trips.add(trip);
    }
}
