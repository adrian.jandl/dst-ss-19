package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.listener.DefaultListener;
import dst.ass1.jpa.model.ILocation;

import javax.persistence.*;

@Entity
@NamedQuery(name = "reachedLocations", query = "select l.locationId FROM Location l " +
        "where exists (select t from Trip t where t.state = 'COMPLETED' and t.destination.locationId = l.locationId) " +
        "or exists (select s.locationId from Trip t join t.stops s where t.state = 'COMPLETED' and s.locationId = l.locationId)")
@EntityListeners(DefaultListener.class)
public class Location implements ILocation {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Long locationId;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getLocationId() {
        return locationId;
    }

    @Override
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }
}
