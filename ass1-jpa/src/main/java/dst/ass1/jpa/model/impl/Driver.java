package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.listener.DefaultListener;
import dst.ass1.jpa.model.IDriver;
import dst.ass1.jpa.model.IEmployment;
import dst.ass1.jpa.model.IVehicle;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@NamedQuery(name = "activeInMultipleOrganizationsDrivers",
        query = "SELECT d from Driver d " +
                "where (select count(e) from d.employments e where e.active = true and e.since <= dateadd('MONTH', -1, current_date )) > :organizations")
@EntityListeners(DefaultListener.class)
public class Driver extends PlatformUser implements IDriver {
    @ManyToOne(targetEntity = Vehicle.class, optional = false)
    private IVehicle vehicle;
    @OneToMany(targetEntity = Employment.class)
    private Collection<IEmployment> employments = new ArrayList<>();

    @Override
    public IVehicle getVehicle() {
        return vehicle;
    }

    @Override
    public void setVehicle(IVehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public Collection<IEmployment> getEmployments() {
        return employments;
    }

    @Override
    public void setEmployments(Collection<IEmployment> employments) {
        this.employments = employments;
    }

    @Override
    public void addEmployment(IEmployment employment) {
        if (this.employments == null) {
            this.employments = new ArrayList<>();
        }
        this.employments.add(employment);
    }
}
