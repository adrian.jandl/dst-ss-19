package dst.ass1.jpa.model;

import java.math.BigDecimal;

public interface IMoney {

    String getCurrency();

    void setCurrency(String currency);

    BigDecimal getValue();

    void setValue(BigDecimal value);
}
