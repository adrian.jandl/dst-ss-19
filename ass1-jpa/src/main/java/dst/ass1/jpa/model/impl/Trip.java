package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

public class Trip implements ITrip {
    private Long id;
    private Date created;
    private Date updated;
    private TripState state;
    private ILocation pickup;
    private ILocation destination;
    private ITripInfo tripInfo;
    private IMatch match;
    private Collection<ILocation> stops = new ArrayList<>();
    private IRider rider;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public Date getUpdated() {
        return updated;
    }

    @Override
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public TripState getState() {
        return state;
    }

    @Override
    public void setState(TripState state) {
        this.state = state;
    }

    @Override
    public ILocation getPickup() {
        return pickup;
    }

    @Override
    public void setPickup(ILocation pickup) {
        this.pickup = pickup;
    }

    @Override
    public ITripInfo getTripInfo() {
        return tripInfo;
    }

    @Override
    public void setTripInfo(ITripInfo tripInfo) {
        this.tripInfo = tripInfo;
    }

    @Override
    public IMatch getMatch() {
        return match;
    }

    @Override
    public void setMatch(IMatch match) {
        this.match = match;
    }

    @Override
    public ILocation getDestination() {
        return destination;
    }

    @Override
    public void setDestination(ILocation destination) {
        this.destination = destination;
    }

    @Override
    public Collection<ILocation> getStops() {
        return stops;
    }

    @Override
    public void setStops(Collection<ILocation> stops) {
        this.stops = stops;
    }

    @Override
    public void addStop(ILocation stop) {
        if (this.stops == null) {
            this.stops = new ArrayList<>();
        }
        this.stops.add(stop);
    }

    @Override
    public IRider getRider() {
        return rider;
    }

    @Override
    public void setRider(IRider rider) {
        this.rider = rider;
        if (rider != null) {
            rider.addTrip(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trip trip = (Trip) o;
        return Objects.equals(id, trip.id) &&
                Objects.equals(created, trip.created) &&
                Objects.equals(updated, trip.updated) &&
                state == trip.state &&
                Objects.equals(pickup, trip.pickup) &&
                Objects.equals(destination, trip.destination) &&
                Objects.equals(tripInfo, trip.tripInfo) &&
                Objects.equals(match, trip.match) &&
                Objects.equals(stops, trip.stops) &&
                Objects.equals(rider, trip.rider);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, created, updated, state, pickup, destination, tripInfo, match, stops, rider);
    }
}
