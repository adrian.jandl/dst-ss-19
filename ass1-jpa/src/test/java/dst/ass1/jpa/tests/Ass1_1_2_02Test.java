package dst.ass1.jpa.tests;

import dst.ass1.jpa.ORMService;
import dst.ass1.jpa.model.IPreferences;
import dst.ass1.jpa.model.IRider;
import dst.ass1.jpa.util.Constants;
import org.hibernate.PropertyValueException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.persistence.PersistenceException;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertFalse;

/**
 * Tests the constraint for IRider email.
 */
public class Ass1_1_2_02Test {

    @Rule
    public ORMService orm = new ORMService();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testUniqueConstraint() {
        new UniqueConstraintTester<>(() -> {
            IRider rider = orm.getModelFactory().createRider();
            rider.setTel("tel");
            IPreferences preferences = orm.getModelFactory().createPreferences();
            orm.getEntityManager().persist(preferences);
            rider.setPreferences(preferences);
            return rider;
        }, e -> e.setEmail("unique@example.com"))
            .run(orm.getEntityManager());
    }

    @Test
    public void testNotNullConstraint() {
        expectedException.expect(PersistenceException.class);
        expectedException.expectCause(isA(PropertyValueException.class));
        expectedException.expectMessage("not-null property");

        IRider e1 = orm.getModelFactory().createRider();
        e1.setEmail(null);
        e1.setTel("tel");
        IPreferences preferences = orm.getModelFactory().createPreferences();
        e1.setPreferences(preferences);
        orm.em().getTransaction().begin();
        orm.em().persist(preferences);
        orm.em().persist(e1);
        orm.em().flush();
    }

    @Test
    public void testRiderEmailNotNullConstraintJdbc() throws SQLException {
        assertFalse(orm.getDatabaseGateway().isNullable(Constants.T_RIDER, Constants.M_RIDER_EMAIL));
    }

}
