package dst.ass1.jpa.tests;

import dst.ass1.jpa.ORMService;
import dst.ass1.jpa.model.IDriver;
import dst.ass1.jpa.util.Constants;
import org.hibernate.PropertyValueException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.persistence.PersistenceException;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertFalse;

/**
 * Tests the IDriver tel not null constraint.
 */
public class Ass1_1_1_04Test {

    @Rule
    public ORMService orm = new ORMService();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testConstraint() {
        expectedException.expect(PersistenceException.class);
        expectedException.expectCause(isA(PropertyValueException.class));
        expectedException.expectMessage("not-null property");

        IDriver e1 = orm.getModelFactory().createDriver();
        e1.setTel(null);
        orm.em().getTransaction().begin();
        orm.em().persist(e1);
        orm.em().flush();
    }

    @Test
    public void testConstraintJdbc() {
        assertFalse(orm.getDatabaseGateway().isNullable(Constants.T_DRIVER, Constants.M_DRIVER_TEL));
    }

}
