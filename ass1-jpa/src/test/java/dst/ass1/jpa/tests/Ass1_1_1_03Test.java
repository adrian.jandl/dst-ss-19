package dst.ass1.jpa.tests;

import dst.ass1.jpa.ORMService;
import dst.ass1.jpa.model.IPreferences;
import dst.ass1.jpa.model.IRider;
import dst.ass1.jpa.util.Constants;
import org.hibernate.PropertyValueException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.persistence.PersistenceException;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertFalse;

/**
 * Tests the IRider tel not null constraint.
 */
public class Ass1_1_1_03Test {

    @Rule
    public ORMService orm = new ORMService();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testConstraint() {
        expectedException.expect(PersistenceException.class);
        expectedException.expectCause(isA(PropertyValueException.class));
        expectedException.expectMessage("not-null property");

        IRider e1 = orm.getModelFactory().createRider();
        e1.setEmail("email@example.com");
        e1.setTel(null);
        IPreferences preferences = orm.getModelFactory().createPreferences();
        e1.setPreferences(preferences);
        orm.em().getTransaction().begin();
        orm.em().persist(preferences);
        orm.em().persist(e1);
        orm.em().flush();
    }

    @Test
    public void testConstraintJdbc() {
        assertFalse(orm.getDatabaseGateway().isNullable(Constants.T_RIDER, Constants.M_RIDER_TEL));
    }
}
