package dst.ass2.service.auth.client.impl;

import dst.ass2.service.api.auth.AuthenticationException;
import dst.ass2.service.api.auth.NoSuchUserException;
import dst.ass2.service.api.auth.proto.*;
import dst.ass2.service.auth.client.AuthenticationClientProperties;
import dst.ass2.service.auth.client.IAuthenticationClient;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import javax.inject.Named;
import javax.inject.Singleton;

@Named
@Singleton
public class GrpcAuthenticationClient implements IAuthenticationClient {
    private AuthServiceGrpc.AuthServiceBlockingStub blockingStub;
    private ManagedChannel channel;

    public GrpcAuthenticationClient() {

    }

    public GrpcAuthenticationClient(AuthenticationClientProperties properties) {
        channel = ManagedChannelBuilder.forAddress(properties.getHost(), properties.getPort()).usePlaintext().build();

        blockingStub = AuthServiceGrpc.newBlockingStub(channel);
    }

    @Override
    public String authenticate(String email, String password) throws NoSuchUserException, AuthenticationException {
        try {
            AuthenticationResponse response =
                    blockingStub.authenticate(AuthenticationRequest.newBuilder().setEmail(email).setPassword(password).build());
            return response.getToken();
        } catch (StatusRuntimeException e) {
            switch (e.getStatus().getCode()) {
                case NOT_FOUND:
                    throw new NoSuchUserException(e.getMessage());
                case UNAUTHENTICATED:
                    throw new AuthenticationException(e.getMessage());
                default:
                    throw e;
            }
        }
    }

    @Override
    public boolean isTokenValid(String token) {
        TokenValidationResponse response =
                blockingStub.validateToken(TokenValidationRequest.newBuilder().setToken(token).build());
        return response.getResponse();
    }

    @Override
    public void close() {
        if (channel != null && !channel.isShutdown()) {
            channel.shutdown();
        }
    }
}
