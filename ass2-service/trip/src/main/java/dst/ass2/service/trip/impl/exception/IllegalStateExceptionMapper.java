package dst.ass2.service.trip.impl.exception;

import org.apache.http.HttpStatus;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class IllegalStateExceptionMapper implements ExceptionMapper<IllegalStateException> {
    @Override
    public Response toResponse(IllegalStateException e) {
        System.out.println("Returning " + HttpStatus.SC_BAD_REQUEST);
        return Response.status(HttpStatus.SC_BAD_REQUEST).build();
    }
}
