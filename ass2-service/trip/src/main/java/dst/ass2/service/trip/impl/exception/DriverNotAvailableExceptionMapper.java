package dst.ass2.service.trip.impl.exception;

import dst.ass2.service.api.trip.DriverNotAvailableException;
import org.apache.http.HttpStatus;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DriverNotAvailableExceptionMapper implements ExceptionMapper<DriverNotAvailableException> {
    @Override
    public Response toResponse(DriverNotAvailableException e) {
        System.out.println("Returning " + HttpStatus.SC_CONFLICT);
        return Response.status(HttpStatus.SC_CONFLICT).build();
    }
}
