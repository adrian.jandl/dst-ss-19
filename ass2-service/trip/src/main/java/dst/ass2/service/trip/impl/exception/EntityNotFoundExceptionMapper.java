package dst.ass2.service.trip.impl.exception;

import dst.ass2.service.api.trip.EntityNotFoundException;
import org.apache.http.HttpStatus;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EntityNotFoundExceptionMapper implements ExceptionMapper<EntityNotFoundException> {
    @Override
    public Response toResponse(EntityNotFoundException e) {
        System.out.println("Returning " + HttpStatus.SC_NOT_FOUND);
        return Response.status(HttpStatus.SC_NOT_FOUND).build();
    }
}
