package dst.ass2.service.trip.impl.exception;

import dst.ass2.service.api.trip.InvalidTripException;
import org.apache.http.HttpStatus;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InvalidTripExceptionMapper implements ExceptionMapper<InvalidTripException> {
    @Override
    public Response toResponse(InvalidTripException e) {
        System.out.println("Returning " + HttpStatus.SC_UNPROCESSABLE_ENTITY);
        return Response.status(HttpStatus.SC_UNPROCESSABLE_ENTITY).build();
    }
}
