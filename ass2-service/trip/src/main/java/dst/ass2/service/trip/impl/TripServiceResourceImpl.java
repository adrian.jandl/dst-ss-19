package dst.ass2.service.trip.impl;

import dst.ass2.service.api.trip.*;
import dst.ass2.service.api.trip.rest.ITripServiceResource;
import org.apache.http.HttpStatus;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Named
@Path("/trips")
public class TripServiceResourceImpl implements ITripServiceResource {
    @Inject
    private ITripService tripService;

    @POST
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTrip(@FormParam("riderId") Long riderId, @FormParam("pickupId") Long pickupId, @FormParam("destinationId") Long destinationId) throws EntityNotFoundException,
            InvalidTripException {
        TripDTO dto = tripService.create(riderId, pickupId, destinationId);
        return Response.ok(dto.getId()).build();
    }

    @PATCH
    @Path("/{id}/confirm")
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirm(@PathParam("id") Long tripId) throws EntityNotFoundException, InvalidTripException {
        tripService.confirm(tripId);
        return Response.ok().build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getTrip(@PathParam("id") Long tripId) throws EntityNotFoundException {
        TripDTO dto = tripService.find(tripId);
        if (dto == null) {
            return Response.status(HttpStatus.SC_NOT_FOUND).build();
        }
        return Response.ok(dto).build();
    }

    @Override
    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTrip(@PathParam("id") Long tripId) throws EntityNotFoundException {
        tripService.delete(tripId);
        return Response.ok().build();
    }

    @Override
    @Path("/{id}/stops")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public Response addStop(@PathParam("id") Long tripId, @FormParam("locationId") Long locationId) throws InvalidTripException,
            EntityNotFoundException {
        TripDTO dto = tripService.find(tripId);
        if (dto == null) {
            return Response.status(HttpStatus.SC_NOT_FOUND).build();
        }
        boolean success = tripService.addStop(dto, locationId);
        if (success) {
            return Response.ok(dto.getFare()).build();
        } else {
            return Response.status(HttpStatus.SC_BAD_REQUEST).build();
        }
    }

    @Override
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}/stops/{locationId}")
    public Response removeStop(@PathParam("id") Long tripId, @PathParam("locationId") Long locationId) throws InvalidTripException,
            EntityNotFoundException {
        TripDTO dto = tripService.find(tripId);
        if (dto == null) {
            return Response.status(HttpStatus.SC_NOT_FOUND).build();
        }
        boolean success = tripService.removeStop(dto, locationId);
        if (success) {
            return Response.ok(dto.getFare()).build();
        } else {
            return Response.status(HttpStatus.SC_BAD_REQUEST).build();
        }
    }

    @Path("/{id}/match")
    @POST
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public Response match(@PathParam("id") Long tripId, MatchDTO matchDTO) throws EntityNotFoundException,
            DriverNotAvailableException {
        tripService.match(tripId, matchDTO);
        return Response.ok().build();
    }

    @POST
    @Path("/{id}/complete")
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public Response complete(@PathParam("id") Long tripId, TripInfoDTO tripInfoDTO) throws EntityNotFoundException {
        tripService.complete(tripId, tripInfoDTO);
        return Response.ok().build();
    }

    @PATCH
    @Path("/{id}/cancel")
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancel(@PathParam("id") Long tripId) throws EntityNotFoundException {
        tripService.cancel(tripId);
        return Response.ok().build();
    }
}
