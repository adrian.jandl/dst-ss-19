package dst.ass2.service.trip.impl;

import dst.ass1.jpa.dao.IDAOFactory;
import dst.ass1.jpa.dao.ILocationDAO;
import dst.ass1.jpa.model.*;
import dst.ass2.service.api.match.IMatchingService;
import dst.ass2.service.api.trip.*;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Date;
import java.time.Instant;
import java.util.stream.Collectors;

@Singleton
@Named
@Transactional
public class TripServiceImpl implements ITripService {
    @Inject
    private IDAOFactory daoFactory;
    @Inject
    private IModelFactory modelFactory;
    @PersistenceContext
    private EntityManager entityManager;
    @Inject
    private IMatchingService matchingService;


    /**
     * Creates and persists a Trip, sets the state to CREATED and calculates an initial fare estimation for
     * the route 'pickupId - destinationId'
     *
     * @param riderId       the id of the rider, who is planning a trip
     * @param pickupId      the id of the pickupId location
     * @param destinationId the id of the destinationId location
     * @return a TripDTO corresponding to the persisted Trip and includes the fare (null if route is invalid)
     * @throws EntityNotFoundException if the rider or one of the locations doesn't exist
     */
    @Override
    public TripDTO create(Long riderId, Long pickupId, Long destinationId) throws EntityNotFoundException {
        ITrip t = modelFactory.createTrip();
        IRider rider = daoFactory.createRiderDAO().findById(riderId);
        ILocationDAO locationDAO = daoFactory.createLocationDAO();
        ILocation pickup = locationDAO.findById(pickupId);
        ILocation destination = locationDAO.findById(destinationId);
        if (rider == null) {
            throw new EntityNotFoundException("could not find rider");
        } else if (pickup == null) {
            throw new EntityNotFoundException("could not find pickup");
        } else if (destination == null) {
            throw new EntityNotFoundException("could not find destination");
        }
        t.setRider(rider);
        t.setPickup(pickup);
        t.setDestination(destination);
        t.setState(TripState.CREATED);

        entityManager.persist(t);

        return getDTOFromEntity(t);
    }

    /**
     * Confirms the given trip (i.e., sets the state of the corresponding trip to QUEUED and
     * puts it into the queue for matching), if possible (i.e., the trip is still in CREATED state)
     *
     * @param tripId the trip to confirm
     * @throws EntityNotFoundException if the trip cannot be found
     * @throws IllegalStateException   in case the trip is not in state CREATED or the rider is null
     * @throws InvalidTripException    in case the fare couldn't be estimated
     */
    @Override
    public void confirm(Long tripId) throws EntityNotFoundException, IllegalStateException, InvalidTripException {
        ITrip t = getTripById(tripId);
        if (t.getState() != TripState.CREATED) {
            throw new IllegalStateException("trip not in state created");
        }
        t.setState(TripState.QUEUED);
        entityManager.persist(t);

        TripDTO tripDTO = getDTOFromEntity(t);
        tripDTO.setFare(matchingService.calculateFare(tripDTO));
        matchingService.queueTripForMatching(tripId);
    }

    /**
     * Creates a match for the given trip and sets the trip's state to MATCHED, if possible (i.e., if the trips is
     * QUEUED). Rolls back transaction in case something goes wrong and re-queues the trip for a new match.
     * You can assume that the locations (pickup, destination and stops) haven't been deleted and will not be deleted
     * during the execution of this method.
     *
     * @param tripId the id of the trip the match will be created for
     * @param match  the match, containing the driver, the vehicle and the fare
     * @throws EntityNotFoundException     in case one of the following doesn't exist anymore: trip, driver or
     *                                     vehicle
     * @throws DriverNotAvailableException in case the driver was assigned in the meantime to another customer
     * @throws IllegalStateException       in case the rider of the trip is null or the trip is not in QUEUED state
     */
    @Override
    @Transactional(value = Transactional.TxType.REQUIRES_NEW, rollbackOn = {EntityNotFoundException.class, DriverNotAvailableException.class, IllegalStateException.class})
    public void match(Long tripId, MatchDTO match) throws EntityNotFoundException, DriverNotAvailableException, IllegalStateException {
        matchingService.queueTripForMatching(tripId);

        ITrip t = getTripById(tripId);
        if (t.getState() != TripState.QUEUED) {
            throw new IllegalStateException("trip is not queued");
        }
        if (t.getRider() == null) {
            throw new IllegalStateException("rider not found");
        }
        long amountOpenTripsForDriver = daoFactory.createTripDAO()
                .findAll()
                .stream()
                .filter(trip -> {
                    if (trip.getMatch() != null) {
                        if (trip.getMatch().getDriver() != null) {
                            if (trip.getMatch().getDriver().getId() != null) {
                                return trip.getMatch().getDriver().getId().equals(match.getDriverId());
                            }
                        }
                    }
                    return false;
                })
                .filter(trip -> trip.getState() != TripState.COMPLETED && trip.getState() != TripState.CANCELLED)
                .count();
        if (amountOpenTripsForDriver > 0) {
            throw new DriverNotAvailableException("Driver not available");
        }
        t.setState(TripState.MATCHED);
        IMatch matchEntity = getEntityFromDTO(match);
        matchEntity.setTrip(t);
        matchEntity.setDate(Date.from(Instant.now()));
        entityManager.persist(matchEntity);
        entityManager.persist(t);
    }

    /**
     * Completes the trip (i. e., persists a TripInfo object and set the trips state to COMPLETED)
     *
     * @param tripId      the id of the trip to complete
     * @param tripInfoDTO the tripInfo to persist
     * @throws EntityNotFoundException in case the trip doesn't exist
     */
    @Override
    @Transactional
    public void complete(Long tripId, TripInfoDTO tripInfoDTO) throws EntityNotFoundException {
        ITrip t = getTripById(tripId);
        t.setState(TripState.COMPLETED);
        entityManager.persist(t);
        ITripInfo tripInfo = getEntityFromDTO(tripInfoDTO);
        tripInfo.setRiderRating(t.getRider().getAvgRating().intValue());
        tripInfo.setDriverRating(t.getMatch().getDriver().getAvgRating().intValue());
        tripInfo.setTrip(t);

        entityManager.persist(tripInfo);
    }

    /**
     * Cancels the given trip (i.e., sets the state to cancelled)
     *
     * @param tripId the trip to cancel
     * @throws EntityNotFoundException in case the trip doesn't exist
     */
    @Override
    public void cancel(Long tripId) throws EntityNotFoundException {
        ITrip t = getTripById(tripId);
        t.setState(TripState.CANCELLED);
        entityManager.persist(t);
    }

    /**
     * Adds the location as a stop if possible (i.e., if the referenced trip is still in the CREATED state and
     * the given location is not already in the list of stops.)
     * As a side effect, the list of the passed TripDTO is modified and the fare freshly estimated.
     * In case the the estimation fails, sets the fare to null.
     * <p>
     * You can assume that the passed TripDTO and the Trip entity have the same values
     *
     * @param trip       the trip
     * @param locationId the location
     * @return true if the stop was added, otherwise false
     * @throws EntityNotFoundException in case the location or trip doesn't exist
     * @throws IllegalStateException   in case the trip isn't longer in the CREATED state
     */
    @Override
    public boolean addStop(TripDTO trip, Long locationId) throws EntityNotFoundException, IllegalStateException {
        ITrip t = getTripById(trip.getId());
        if (t.getState() != TripState.CREATED) {
            throw new IllegalStateException("trip is not in state " + TripState.CREATED);
        }

        ILocation toAdd = getLocationById(locationId);
        if (t.getStops().contains(toAdd)) {
            return false;
        } else {
            t.addStop(toAdd);
            entityManager.persist(t);
            trip.getStops().add(toAdd.getLocationId());
            try {
                trip.setFare(matchingService.calculateFare(trip));
            } catch (InvalidTripException e) {
                trip.setFare(null);
            }
            return true;
        }
    }

    /**
     * Removes the location from the stops only (i.e., if the referenced trip is still in the CREATED state).
     * As a side effect, the list of the passed TripDTO is modified and the fare freshly estimated.
     * In case the the estimation fails, sets the fare to null.
     * <p>
     * You can assume that the passed TripDTO and the Trip entity have the same values
     *
     * @param trip       the trip
     * @param locationId the location to remove
     * @return true if the trip was removed, otherwise false (for example when the location wasn't added to the stops)
     * @throws EntityNotFoundException in case the location or trip doesn't exist
     * @throws IllegalStateException   in case the  trip isn't longer in the CREATED state
     */
    @Override
    public boolean removeStop(TripDTO trip, Long locationId) throws EntityNotFoundException, IllegalStateException {
        ITrip t = getTripById(trip.getId());
        if (t.getState() != TripState.CREATED) {
            throw new IllegalStateException("trip is not in state " + TripState.CREATED);
        }
        ILocation toRemove = getLocationById(locationId);
        if (t.getStops().contains(toRemove)) {
            t.getStops().remove(toRemove);
            try {
                trip.setFare(matchingService.calculateFare(trip));
            } catch (InvalidTripException e) {
                trip.setFare(null);
            }
            entityManager.persist(t);
            trip.getStops().remove(toRemove.getLocationId());
            return true;
        } else {
            return false;
        }
    }

    /**
     * Removes the trip with the given id.
     *
     * @param tripId the id of the trip to remove
     * @throws EntityNotFoundException in case the trip doesn't exist
     */
    @Override
    public void delete(Long tripId) throws EntityNotFoundException {
        ITrip trip = daoFactory.createTripDAO().findById(tripId);
        if (trip == null) {
            throw new EntityNotFoundException("trip not found");
        }
        entityManager.remove(trip);
    }

    /**
     * Finds the trip with the given id and returns it as DTO, including the latest fare estimation.
     *
     * @param tripId the id of the trip
     * @return if found the DTO, otherwise null
     */
    @Override
    public TripDTO find(Long tripId) {
        ITrip t = daoFactory.createTripDAO().findById(tripId);
        if (t == null) {
            return null;
        }

        return getDTOFromEntity(t);
    }

    private TripDTO getDTOFromEntity(ITrip trip) {
        TripDTO dto = new TripDTO();
        dto.setId(trip.getId());
        dto.setDestinationId(trip.getDestination().getId());
        dto.setPickupId(trip.getPickup().getId());
        dto.setRiderId(trip.getRider().getId());
        dto.setStops(trip.getStops().stream().map(ILocation::getId).collect(Collectors.toList()));
        try {
            dto.setFare(matchingService.calculateFare(dto));
        } catch (InvalidTripException e) {
            e.printStackTrace();
        }

        return dto;
    }

    private ITripInfo getEntityFromDTO(TripInfoDTO dto) {
        ITripInfo tripInfo = modelFactory.createTripInfo();
        tripInfo.setCompleted(dto.getCompleted());
        tripInfo.setDistance(dto.getDistance());
        tripInfo.setTotal(getEntityFromDTO(dto.getFare()));

        return tripInfo;
    }

    private IMoney getEntityFromDTO(MoneyDTO dto) {
        IMoney money = modelFactory.createMoney();
        money.setValue(dto.getValue());
        money.setCurrency(dto.getCurrency());

        return money;
    }

    private ITrip getTripById(Long tripId) throws EntityNotFoundException {
        ITrip t = daoFactory.createTripDAO().findById(tripId);
        if (t == null) {
            throw new EntityNotFoundException("trip not found");
        }

        return t;
    }

    private ILocation getLocationById(Long locationId) throws EntityNotFoundException {
        ILocation location = daoFactory.createLocationDAO().findById(locationId);
        if (location == null) {
            throw new EntityNotFoundException("location not found");
        }

        return location;
    }

    private IMatch getEntityFromDTO(MatchDTO dto) throws EntityNotFoundException {
        IMatch match = modelFactory.createMatch();
        IDriver driver = daoFactory.createDriverDAO().findById(dto.getDriverId());
        if (driver == null) {
            throw new EntityNotFoundException("driver not found");
        }
        match.setDriver(driver);
        match.setFare(getEntityFromDTO(dto.getFare()));
        IVehicle vehicle = daoFactory.createVehicleDAO().findById(dto.getVehicleId());
        if (vehicle == null) {
            throw new EntityNotFoundException("vehicle not found");
        }
        match.setVehicle(vehicle);

        return match;
    }
}
