package dst.ass2.service.auth.grpc;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;

@Named
@Singleton
public class GrpcServerRunnerImpl implements IGrpcServerRunner {
    @Inject
    private GrpcServerProperties properties;
    @Inject
    private GrpcAuthService grpcAuthService;
    private Server server;
    private int port;

    public GrpcServerRunnerImpl() {

    }

    @PostConstruct
    private void setup() {
        ServerBuilder serverBuilder = ServerBuilder.forPort(properties.getPort());
        server = serverBuilder.addService(grpcAuthService).build();
        port = properties.getPort();
    }

    /**
     * Starts the gRPC server.
     *
     * @throws IOException start error
     */
    @Override
    public void run() throws IOException {
        server.start();
        System.out.println("Started Grpc Server on port " + port);
    }
}
