package dst.ass2.service.auth.grpc;

import com.google.rpc.Code;
import com.google.rpc.Status;
import dst.ass2.service.api.auth.AuthenticationException;
import dst.ass2.service.api.auth.IAuthenticationService;
import dst.ass2.service.api.auth.NoSuchUserException;
import dst.ass2.service.api.auth.proto.*;
import io.grpc.protobuf.StatusProto;
import io.grpc.stub.StreamObserver;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Named
@Singleton
public class GrpcAuthService extends AuthServiceGrpc.AuthServiceImplBase {
    @Inject
    private IAuthenticationService authenticationService;


    /**
     * @param request
     * @param responseObserver
     */
    @Override
    public void authenticate(AuthenticationRequest request, StreamObserver<AuthenticationResponse> responseObserver) {
        try {
            String uuid = authenticationService.authenticate(request.getEmail(), request.getPassword());
            AuthenticationResponse response =
                    AuthenticationResponse.newBuilder().setToken(uuid).build();
            responseObserver.onNext(response);
        } catch (NoSuchUserException e) {
            Status status = Status.newBuilder()
                    .setCode(Code.NOT_FOUND.getNumber())
                    .setMessage(e.getMessage())
                    .build();
            responseObserver.onError(StatusProto.toStatusException(status));
        } catch (AuthenticationException e) {
            Status status = Status.newBuilder()
                    .setCode(Code.UNAUTHENTICATED.getNumber())
                    .setMessage(e.getMessage())
                    .build();
            responseObserver.onError(StatusProto.toStatusException(status));
        }
        responseObserver.onCompleted();
    }

    /**
     * @param request
     * @param responseObserver
     */
    @Override
    public void validateToken(TokenValidationRequest request, StreamObserver<TokenValidationResponse> responseObserver) {
        boolean result = authenticationService.isValid(request.getToken());
        responseObserver.onNext(TokenValidationResponse.newBuilder().setResponse(result).build());
        responseObserver.onCompleted();
    }
}
