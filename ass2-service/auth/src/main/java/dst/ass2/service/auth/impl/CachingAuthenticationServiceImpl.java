package dst.ass2.service.auth.impl;

import dst.ass1.jpa.dao.IDAOFactory;
import dst.ass1.jpa.model.IRider;
import dst.ass2.service.api.auth.AuthenticationException;
import dst.ass2.service.api.auth.NoSuchUserException;
import dst.ass2.service.auth.ICachingAuthenticationService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
@Named
public class CachingAuthenticationServiceImpl implements ICachingAuthenticationService {
    private Map<String, byte[]> emailPasswordMap = new ConcurrentHashMap<>();
    private Map<String, String> tokenEmailMap = new HashMap<>();
    private MessageDigest messageDigest;
    @Inject
    private IDAOFactory daoFactory;
    @PersistenceContext
    private EntityManager entityManager;

    public CachingAuthenticationServiceImpl() throws NoSuchAlgorithmException {
        messageDigest = MessageDigest.getInstance("SHA1");
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * Instead of checking database records directly, the method first checks the cache for existing users. If the user
     * is not in the cache, then the service checks the database for the given email address, and updates the cache if
     * necessary.
     * </p>
     *
     * @param email
     * @param password
     */
    @Override
    public String authenticate(String email, String password) throws NoSuchUserException, AuthenticationException {

        if (!emailPasswordMap.containsKey(email)) {

            if (daoFactory.createRiderDAO().findByEmail(email) != null) {
                loadData();
            } else {
                throw new NoSuchUserException("user does not exist");
            }
        }

        if (!Arrays.equals(emailPasswordMap.get(email), messageDigest.digest(password.getBytes()))) {
            throw new AuthenticationException("could not authenticate");
        }

        String token = String.valueOf(UUID.randomUUID());
        tokenEmailMap.put(token, email);

        return token;
    }

    /**
     * Loads user data from the database into memory.
     */
    @Override
    @PostConstruct
    public void loadData() {
        daoFactory.createRiderDAO().findAll().forEach(r -> emailPasswordMap.put(r.getEmail(), r.getPassword()));
    }

    /**
     * Clears the data cached from the database.
     */
    @Override
    public void clearCache() {
        emailPasswordMap.clear();
        tokenEmailMap.clear();
    }

    /**
     * Changes the password of the given user in the database. Also updates the in-memory cache in a thread-safe way.
     *
     * @param email       the user email
     * @param newPassword the new password in plain text.
     * @throws NoSuchUserException if the given user was not found
     */
    @Override
    public void changePassword(String email, String newPassword) throws NoSuchUserException {
        IRider rider = daoFactory.createRiderDAO().findByEmail(email);
        if (rider == null) {
            throw new NoSuchUserException("user not found");
        }
        byte[] password = messageDigest.digest(newPassword.getBytes());
        rider.setPassword(password);
        entityManager.persist(rider);

        emailPasswordMap.put(email, password);
    }

    /**
     * Returns the user that is associated with this token. Returns null if the token does not exist.
     *
     * @param token an authentication token previously created via {@link #authenticate(String, String)}
     * @return the user's email address or null
     */
    @Override
    public String getUser(String token) {
        return tokenEmailMap.get(token);
    }

    /**
     * Checks whether the given token is valid (i.e., was issued by this service and has not been invalidated).
     *
     * @param token the token to validate
     * @return true if the token is valid, false otherwise
     */
    @Override
    public boolean isValid(String token) {
        return tokenEmailMap.containsKey(token);
    }

    /**
     * Invalidates the given token, i.e., removes it from the cache. Returns false if the token did not exist.
     *
     * @param token the token to invalidate
     * @return true if the token existed and was successfully invalidated, false otherwise
     */
    @Override
    public boolean invalidate(String token) {
        if (tokenEmailMap.containsKey(token)) {
            tokenEmailMap.remove(token);
            return true;
        } else {
            return false;
        }
    }

}
