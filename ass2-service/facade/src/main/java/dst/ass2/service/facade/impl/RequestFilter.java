package dst.ass2.service.facade.impl;

import dst.ass2.service.auth.client.IAuthenticationClient;
import io.netty.handler.codec.http.HttpResponseStatus;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@Filtered
public class RequestFilter implements ContainerRequestFilter {
    @Inject
    private IAuthenticationClient authenticationClient;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        String authorization = containerRequestContext.getHeaderString("Authorization");
        String token = authorization.replace("Authorization: Bearer ", "");
        if (!authenticationClient.isTokenValid(token)) {
            containerRequestContext.abortWith(Response.status(HttpResponseStatus.UNAUTHORIZED.code()).build());
        }
    }
}