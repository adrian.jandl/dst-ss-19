package dst.ass2.service.facade.impl;

import dst.ass2.service.api.trip.*;
import dst.ass2.service.api.trip.rest.ITripServiceResource;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Named
@Filtered
public class TripResourceImpl implements ITripServiceResource {

    private WebTarget webTarget;

    @Inject
    public TripResourceImpl(URI tripServiceUri) {
        webTarget = ClientBuilder.newClient().target(tripServiceUri);
    }

    @Override
    public Response createTrip(Long riderId, Long pickupId, Long destinationId) throws EntityNotFoundException, InvalidTripException {
        return webTarget.path("/trips")
                .queryParam("riderId", riderId)
                .queryParam("pickupId", pickupId)
                .queryParam("destinationId", destinationId)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get();
    }

    @Override
    public Response confirm(Long tripId) throws EntityNotFoundException, InvalidTripException {
        return webTarget.path("/trips/" + tripId + "/confirm")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .method("PATCH");
    }

    @Override
    public Response getTrip(Long tripId) throws EntityNotFoundException {
        return webTarget.path("/trips/" + tripId)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get();
    }

    @Override
    public Response deleteTrip(Long tripId) throws EntityNotFoundException {
        return webTarget.path("/trips/" + tripId)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .delete();
    }

    @Override
    public Response addStop(Long tripId, Long locationId) throws InvalidTripException, EntityNotFoundException {
        return webTarget.path("/trips/" + tripId + "/stops")
                .queryParam("locationId", locationId)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_JSON_TYPE));
    }

    @Override
    public Response removeStop(Long tripId, Long locationId) throws InvalidTripException, EntityNotFoundException {
        return webTarget.path("/trips/" + tripId + "/stops/" + locationId)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .delete();
    }

    @Override
    public Response match(Long tripId, MatchDTO matchDTO) throws EntityNotFoundException, DriverNotAvailableException {
        return webTarget.path("/trips/" + tripId + "/match")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_JSON_TYPE));
    }

    @Override
    public Response complete(Long tripId, TripInfoDTO tripInfoDTO) throws EntityNotFoundException {
        return webTarget.path("/trips/" + tripId + "/complete")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(null, MediaType.APPLICATION_JSON_TYPE));
    }

    @Override
    public Response cancel(Long tripId) throws EntityNotFoundException {
        return webTarget.path("/trips/" + tripId + "/cancel")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .method("PATCH");
    }
}
