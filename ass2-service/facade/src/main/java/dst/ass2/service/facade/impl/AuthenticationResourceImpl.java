package dst.ass2.service.facade.impl;

import dst.ass2.service.api.auth.AuthenticationException;
import dst.ass2.service.api.auth.NoSuchUserException;
import dst.ass2.service.api.auth.rest.IAuthenticationResource;
import dst.ass2.service.auth.client.IAuthenticationClient;
import io.netty.handler.codec.http.HttpResponseStatus;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/auth")
@Named
public class AuthenticationResourceImpl implements IAuthenticationResource {
    @Inject
    private IAuthenticationClient authenticationClient;

    @Override
    @POST
    @Path("/authenticate")
    public Response authenticate(@FormParam("email") String email, @FormParam("password") String password) throws NoSuchUserException, AuthenticationException {
        try {
            String token = authenticationClient.authenticate(email, password);
            return Response.ok(token).build();
        } catch (NoSuchUserException e) {
            return Response.status(HttpResponseStatus.BAD_REQUEST.code()).build();
        } catch (AuthenticationException e) {
            return Response.status(HttpResponseStatus.UNAUTHORIZED.code()).build();
        }
    }
}
