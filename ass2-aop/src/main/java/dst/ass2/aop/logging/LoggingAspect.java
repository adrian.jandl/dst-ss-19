package dst.ass2.aop.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.lang.reflect.Field;
import java.util.logging.Logger;

@Aspect
public class LoggingAspect {

    @Around("execution(* dst.ass2.aop.IPluginExecutable.execute(..))&&!@annotation(Invisible)")
    public void logAroundAllMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        Object target = joinPoint.getTarget();
        Field[] fields = target.getClass().getDeclaredFields();
        Logger l = null;
        for (Field f : fields) {
            f.setAccessible(true);
            if (Logger.class.isAssignableFrom(f.getType())) {
                l = (Logger) f.get(target);
            }
        }
        String logString = "****LoggingAspect.logAroundAllMethods() : " + joinPoint.getTarget().getClass() + ": Before Method Execution";

        if (l != null) {
            l.info(logString);
        } else {
            System.out.println(logString);
        }

        joinPoint.proceed();

        logString = "****LoggingAspect.logAroundAllMethods() : " + joinPoint.getTarget().getClass() + ": After Method Execution";
        if (l != null) {
            l.info(logString);
        } else {
            System.out.println(logString);
        }
    }

}
