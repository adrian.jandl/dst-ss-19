package dst.ass2.aop.impl;

import dst.ass2.aop.IPluginExecutable;
import dst.ass2.aop.IPluginExecutor;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.*;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PluginExecutorImpl implements IPluginExecutor, Runnable {
    private static final String PATH_TO_DIR = "";
    private WatchService ws;
    private Map<Path, WatchKey> pathToWatchKey = new HashMap<>();
    private boolean running;
    private ExecutorService threadPool = Executors.newFixedThreadPool(10);

    public PluginExecutorImpl() throws IOException {
        ws = FileSystems.getDefault().newWatchService();
        running = false;
    }

    /**
     * Adds a directory to monitor.
     * May be called before and also after start has been called.
     *
     * @param dir the directory to monitor.
     */
    @Override
    public void monitor(File dir) {
        pathToWatchKey.put(dir.toPath(), null);
        if (running) {
            startAndRegisterWatchKey(dir.toPath());
        }
    }

    private void startAndRegisterWatchKey(Path p) {
        try {
            WatchKey wk = p.register(ws, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);

            pathToWatchKey.put(p, wk);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Stops monitoring the specified directory.
     * May be called before and also after start has been called.
     *
     * @param dir the directory which should not be monitored anymore.
     */
    @Override
    public void stopMonitoring(File dir) {
        Path p = dir.toPath();
        if (pathToWatchKey.containsKey(p)) {
            if (running) {
                pathToWatchKey.get(p).cancel();
            }
            pathToWatchKey.remove(p);
        }
    }

    /**
     * Starts the plugin executor.
     * All added directories will be monitored and any .jar file processed.
     * If there are any {@link IPluginExecutable} implementations,
     * they are executed within own threads.
     */
    @Override
    public void start() {
        running = true;
        pathToWatchKey.forEach((path, watchKey) -> startAndRegisterWatchKey(path));

        new Thread(this).start();
    }

    /**
     * Stops the plugin executor.
     * The monitoring of directories and the execution
     * of the plugins should stop as soon as possible.
     */
    @Override
    public void stop() {
        if (running) {
            pathToWatchKey.forEach((path, watchKey) -> watchKey.cancel());
        }
        running = false;
    }

    @Override
    public void run() {
        WatchKey key = null;


        while (true) {
            for (Map.Entry<Path, WatchKey> wk : pathToWatchKey.entrySet()) {
                if (wk == null) {
                    continue;
                }
                for (WatchEvent<?> event : wk.getValue().pollEvents()) {
                    System.out.println(
                            "Event kind:" + event.kind() +
                                    ". File affected: " + event.context() + ".");
                    if (event.context().toString().endsWith(".jar")) {
                        WatchEvent<Path> ev = (WatchEvent<Path>) event;

                        try {
                            File f = new File(wk.getKey() + "/" + event.context());
                            JarFile jarFile = new JarFile(f);
                            Enumeration<JarEntry> entry = jarFile.entries();
                            while (entry.hasMoreElements()) {
                                JarEntry e = entry.nextElement();
                                String x = e.getName();
                                if (x.contains("class")) {

                                    String className = e.getName().substring(0, e.getName().length() - 6);
                                    className = className.replace("/", ".");
                                    URLClassLoader child = new URLClassLoader(
                                            new URL[]{f.toURI().toURL()},
                                            this.getClass().getClassLoader()
                                    );
                                    Class classToLoad = Class.forName(className, true, child);

                                    Class[] interfaces = classToLoad.getInterfaces();
                                    if (Arrays.asList(interfaces).contains(IPluginExecutable.class)) {
                                        Object o = classToLoad.newInstance();
                                        Method m = classToLoad.getMethod("execute", null);
                                        threadPool.execute(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    m.invoke(o, null);
                                                } catch (IllegalAccessException | InvocationTargetException e1) {
                                                    e1.printStackTrace();
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        } catch (IOException | ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
