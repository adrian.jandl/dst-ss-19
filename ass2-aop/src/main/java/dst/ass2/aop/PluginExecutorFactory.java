package dst.ass2.aop;

import dst.ass2.aop.impl.PluginExecutorImpl;

import java.io.IOException;

public class PluginExecutorFactory {

    public static IPluginExecutor createPluginExecutor() {
        try {
            return new PluginExecutorImpl();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
