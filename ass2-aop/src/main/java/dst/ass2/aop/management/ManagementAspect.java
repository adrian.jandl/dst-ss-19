package dst.ass2.aop.management;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;

@Aspect
public class ManagementAspect {
    private ThreadLocal<Timer> timerThreadLocal = new ThreadLocal<>();

    @Before("execution(* dst.ass2.aop.IPluginExecutable..*(..))&&@annotation(Timeout)")
    public void before(JoinPoint joinPoint) {
        Object target = joinPoint.getTarget();
        MethodSignature ms = (MethodSignature) joinPoint.getSignature();
        Method m = ms.getMethod();
        Timeout t = m.getAnnotation(Timeout.class);

        if (t != null) {
            timerThreadLocal.set(new Timer(true));

            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {

                    try {
                        Method m = target.getClass().getMethod("interrupted", null);
                        m.invoke(target, null);
                    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }

                }
            };
            timerThreadLocal.get().schedule(timerTask, t.value());
        }
    }

    @After("execution(* dst.ass2.aop.IPluginExecutable.execute(..))&&@annotation(Timeout)")
    public void after(JoinPoint joinPoint) {
        Timer t = timerThreadLocal.get();
        if (t != null) {
            t.cancel();
        }
    }
}
